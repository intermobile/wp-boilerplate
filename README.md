# WP Boilerplate

This is a website project based on **WordPress** and developed for **\_B_CLIENT_NAME**.

Website: [\_B_SITE_URL](_B_SITE_URI)

## Summary

- [Repository](#user-content-repository)
- [Technologies](#user-content-technologies)
- [Requirements](#user-content-requirements)
- [Instalation](#user-content-instalation)
- [Git Branches](#user-content-branches)
- [Code Formatting](#user-content-formatting)
- [Gulp Tasks](#user-content-gulp-tasks)
- [Theme structure](#user-content-theme-structure)
- [Javascript](#user-content-javascript)
- [SCSS](#user-content-scss)
- [Localization](#user-content-localization-translations)
- [More info](#user-content-more-info)

## Repository

This git repository holds the WordPress root folder without the WordPress core files, including only the `themes`, `plugins` and `languages` folders. That means plugins are updated locally from the git repository.

> The major part of this documentation is about the `_B_THEME_NAME` theme, that is where really lives the website code, so consider it to the next sections of this doc.

#### .gitignore

We have two `.gitignore` files, one for the WordPress root, and other to our `_B_THEME_NAME` theme.

`CSS` and `JS` dist files are included in the repository and need to be compiled locally and added on each commit to the main branches.

## Technologies

The project is mainly based on the following technologies:

- [WordPress](https://developer.wordpress.org/) - CMS and Back-End Logic
- [Composer](https://getcomposer.org/) - PHP dependency manager (Back-End)
- [Advanced Custom Fields](https://www.advancedcustomfields.com/) - WP plugin used for registering custom fields in the CMS
- [NPM](https://www.npmjs.com/) - JS dependency manager (Front-End)
- [Bootstrap 5](https://getbootstrap.com/) - CSS framework
- [SASS](https://sass-lang.com/) - CSS preprocessor
- [jQuery](https://jquery.com/) - JS framework
- [Webpack](https://webpack.js.org/) - JS bundler
- [Babel](https://babeljs.io/) - ES6 JS compiler
- [Gulp](https://gulpjs.com/) - Task runner
- [Swiper](https://swiperjs.com/) - JS library for carousels and sliders
- [Axios](https://github.com/axios/axios) - AJAX library for Javascript
- [Anime](https://animejs.com/) - JS animation framework

In addition we may also use other external JS plugins for specific features.

## Requirements

To run this project on your local machine (or server), you will need:

- [Apache Server](https://httpd.apache.org/download.cgi)
- [PHP](https://www.php.net/downloads) (`7`+)
- [MySQL Server](https://dev.mysql.com/downloads/mysql/) (`5.x`)
- [Composer](https://getcomposer.org/)
- [Node.js](https://nodejs.org/en/) (including [npm](https://www.npmjs.com/))
- [Gulp CLI](https://gulpjs.com/) (installed globally via npm) (`2.x`)
- [Plop](https://plopjs.com/) (installed globally via npm) (`2.x`)

## Installation

There are 2 ways to set up the project in you local environment:

### Using the `gulp setup` task

1. Clone the repository on your machine;
2. In a terminal, navigate to the main theme folder (`_B_THEME_NAME`) and run `npm i` to install all JS dependencies;
3. Once finished, still in the terminal, run `gulp setup` (You need to have `gulp-cli` globally installed), to download WordPress, create local database, and install dependencies;
4. After this, run the task again as an admin user (`sudo` in Unix systems) to set up a virtual host for the local domain;
5. If you were able to run all steps above successfuly, you can now import the database (that should be provided to you) into the `wp-boilerplate_local` database, restart apache, and go to the `http://wp-boilerplate.local/` URL.

### Or manually

1. Clone the repository on your machine;
2. In a terminal, navigate to the main theme folder (`_B_THEME_NAME`) and run `npm i` to install all JS dependencies, and `composer install` to install all PHP dependencies;;
3. Download the [latest version](https://wordpress.org/latest.zip) of WordPress, and copy all its content (except the `wp-content` folder) to the root of this project;
4. Add the following rule to your hosts file `127.0.0.1 wp-boilerplate.local`;
5. Setup a virtual host in Apache to the URL `http://wp-boilerplate.local/`;
6. Copy the content of the `.gulp/templates/wpconfig` file and create a `wp-config.php` file at the root of the project, inserting your MySQL credentials;
7. Create a new MySQL database named `wp-boilerplate_local` and import the database (that should be provided to you).
8. Restart apache and open the new local URL in your browser;

## Branches

At the time of the creation of this documentation we have 2 main branches, each one to a web environment:

1. `homolog` - Development environment
2. `master` - Production environment

Other branches are used to specific features and modifications.

## Formatting

For code formatting, this project uses:

- [Prettier](https://prettier.io/) - That defines the formatting rules for `js`, `scss`, `css`, `json` and `html` files.
- [PHP Code Beautifier and Fixer](https://phpqa.io/projects/phpcbf.html) - Also known as **phpcbf**, this controls how `php` files are formatted, by also fixing some possible issues accordingly to WordPress coding standard, using **PHP Code Sniffer**.

### VS Code

> For Front-End development, we highly recommend using the [Visual Studio Code](https://code.visualstudio.com/) IDE as this project already comes with some predefined rules and extension recommendations for this editor.

Specifically for VS Code, we have:

- [Format HTML in PHP](https://marketplace.visualstudio.com/items?itemName=rifi2k.format-html-in-php) - A better code formatter for `html` inside `php` files.

> We high recommend opening the project through the **Workspace** file (`_B_FOLDER_NAME.code-workspace`) file, that comes with some formatting settings and recommended extensions.

## Gulp Tasks

The Gulp tasks are one of the main tools that the front-end developer can use.

To run a task just open a terminal, go to the `_B_THEME_NAME` theme directory and type the desired task command.

Some basic configurations for each tasks are defined in the `config.json` file (mainly file location).

See all the available tasks below:

> Make sure you have all needed dependencies installed to run the gulp tasks, using `npm install` or `npm i`.

> Flags can be passed with the **full** or **short** version, i.e. `--watch` or `-w`. Both ways work the same.

### `gulp sass`

Compiles all **SCSS** entry files (theme, layouts, and components).

#### Available Flags

- `--watch`, `-w` - Watch for changes to the files and rerun the task when a file is modified (saved).
- `--dev`, `-d` - Compiles with as unminified and with sourcemaps.

> While watching for changes, this task tries to compile only relevant files based on the location of the changed file. In this case, the `-p` flag is not used.

### `gulp js`

Compiles the **JS** files from each layout.

#### Available Flags

- `--watch`, `-w` - Watch for changes to the files and rerun the task when a file is modified (saved).
- `--dev`, `-d` - Compiles with as uncrompressed and with sourcemaps.
- `--page`, `-p` - Define a specific page to compile the script, instead of all pages.

### `gulp` (Default)

Run the `sass` and `js` tasks, with same available flags.

### `gulp setup`

Does a series of tasks for setting up the project in your local machine.

> Some of its tasks may require specific definitions from you local environment, like MySQL credentials, hosts file location and more.

#### **As non-admin**

- Downloads WordPress core files from latest version.
- Setup `wp-config.php` and `.htaccess`
- Create a MySQL database
- Install PHP dependencies with Composer

#### **As admin/sudo**

- Add rule for the local host domain
- Add rule for a virtual host for Apache server

### `gulp pot`

Get all _gettext_ strings in all PHP files and create a `.pot` file to be used for translations.

### `gulp mo`

Compile a binary version of each translation file (`.po`).

### `gulp favicons`

Generate **favicon** image variations for multiple platforms based on a main `favicon.png` image (`./assets/img/favicons.png`), and creates an html file with metas for the favicons.

> Source image should be at least `512`x`512`px, but ideally `1024`x`1024`px.

### `gulp iconfont`

Get all SVG files from a folder and generates font files (`.svg`, `.ttf`, `.woff`, `.woff2`) for a new icons' font.

### `gulp webfonts`

Get fonts on TrueType format (`.ttf`) and generates equivalent fonts for web (`.woff`, `.woff2`) in the same font family folder.

#### Available Flags

- `--family`, `-F` (_mandatory_) - Name of the folder where the `.ttf` fonts are located.

> The source fonts should be placed at the `<family>/ttf/` subfolder.

> Note that the short version of the flag uses an uppercase letter (`-F`) as `-f` is already reserved for the gulp CLI.

### config.json

Every task uses information from the `config.json`, like paths relative to the theme root directory. If you want to modify or consult the default directory for any gulp task you may want to check this file.

## Theme structure

Since the project is based mainly on _**WordPress**_ you need to know WordPress theme development to be able to work with it.

Our `_B_THEME_NAME` theme, depends natively on 2 packages (_Composer_):

- [Autoload](https://getcomposer.org/doc/01-basic-usage.md#autoloading) - That is used to automatically import any PHP class that is used in the theme's code.
- [WP Solidify](https://gitlab.com/intermobile/wp-solidify) - Resposible for the core functionalities behind the back-end code, and the base PHP classes that are used along the whole theme.

In a nutshell, at the root of the theme, we have:

- `funcions.php` - Where some definitions are made to the project, creating an instance of WP Solidify.
- `PHP files at theme root` - Most PHP files in our theme's root directory are entry files for each type of page. This files follows the WordPress native [Template Hierarchy](https://developer.wordpress.org/themes/basics/template-hierarchy/).
- `components` - Have all our PHP components, sections and widgets used in the website, including their template (`php`), scripts (`js`) and styles (`scss`).
- `pages` - Have the template and entry JS & CSS for each type of page in the theme.
- `classes` - Most logic of the theme lives in this folder. Every file in this folder is a PHP class that controls an aspect of the site. Most of these classes somehow inherit from the `Registrable` or `Renderable` classes from **WP Solidify**. These files are organized by the following subfolders:
  - `Components` - Classes of type `Renderable` that controls each component of the page;
  - `FieldGroups` - Classes of type `Registrable` that are used for registering ACF groups.
  - `Helpers` - Classes of reusable functions for the project's logic. - `Hooks` - Classes of type `Registrable` that where functions are **action** and **filters** are registered for WordPress [hooks](https://developer.wordpress.org/plugins/hooks/).
  - `Pages` - Classes of type `Renderable` that represents each type of page available for the site.
  - `PostTypes` - Classes of type `Registrable` that are used for registering new [Custom Post Type](https://wordpress.org/support/article/post-types/#custom-post-types) needed in the site.
  - `Taxonomies` - Classes of type `Registrable` that are used for registering new [Taxonomies](https://wordpress.org/support/article/taxonomies/) needed in the site.
- `assets` - Contains our theme **images**, **fonts**, core **JS** and **SCSS** files, and the **JS** & **CSS** dist files for each type of page.
- `languages` - Where we have the `.po` and `.mo` files with translations for different languages.

### Layouts

We use the [WordPress template hierarchy](https://developer.wordpress.org/themes/basics/template-hierarchy/) to define which route uses which file. All pages are read from the root directory but its content are placed in the `./layouts/` folder, and sequentially in the `./components/` folder. So if you need to change any content in a page, you will probably find what you need in these two folders, or at their respective classes at `./classes/Layouts/` or `./classes/Components/`.

Each layout uses an independent `js` and `css` dist file. These files are named with the name of the layout, and are automatically imported when printing the layout class.

### Classes

For a **component-based** theme structure, we use PHP classes to render components and layouts, while the template usually receives the data from the class and render it as needed.

## Javascript

The scripts in the site uses **jQuery** as our main JS framework, and **ES6+** features.

> Make sure you have all needed dependencies, with `npm install`, before compiling the JS files.

There is an entry JS file for each layout that is located on the respective folder of the layout at `./layouts/`. These entry files are where each component script is imported and passed to a new instance of the `Page` class.

The scripts for each component is located on its respective folder at `./components/` and is imported on the page entry files.

Additionally, at the `./assets/js/` directory we have the following folders:

- `core` - that contains some core files, like the `Essentials` component that includes any component needed on all pages, and behaviour components, like the `IE` component, that controls some aspects for the _Internet Explorer_ browser specifically.
- `dist` - where all final bundled files are generated. There is a file for each type of page in the site.
- `libs` - where we store files for external libraries that are not bundled in the dist files, but imported directly in the page.
- `utils` - that contains our base JS classes (`Page` and `Component`) and the `globals.js` file that have some util jQuery references (like `win`, `body` and `doc`) or shared functions.

> By default the JS gulp task generates a production optimized bundle. To build the JS with source maps and uncompressed (for debug purposes) use the `--dev` (or `-d`) flag after in the command. Just be careful to not send this non-optimized file to the production environment.

Our Javascript structure is based on these main concepts:

- Each page JS includes its needed components and results into a dist file at `./assets/js/dist/`;
- In these entry files we run the code to be executed on **loading**, **scrolling** and **resizing** the page.
- The `Page` class that is instantiated on each entry file, receives the instance of each component so that it can call the main functions available at the `Component` class (`pageLoaded()`, `pageScrolled()`, `windowResized()` and `scriptLoaded()`).
- All components are inherit from the `Component` class.
- This `Component` class exposes some functions that should be used on each component, that are then handled in the `Page` instance.
- Some of these funcions already pass util values as arguments like, `scrollPos`, `winWidth` and `winHeight`. So use them when needed.
- The `webpack.config.js` contains our **webpack** configuration that defines how the bundle will be compiled, which libraries should be embeded (_plugins_) and which libraries should be used in the JS files but imported externally (_externals_).

Beyond that you only need to know _jQuery_, and a little bit of _Webpack_ if you want to add more libraries or to do anything that the project ain't prepared for.

For more information on writing JS for this theme, check the **Javascript Guide** [README.md](wp-content/themes/_B_THEME_NAME/assets/js/README.md)`.

## SCSS

We use the _**SASS**_ pre-processor to write the CSS styles for pages and components, and **Bootstrap 4** as our main CSS framework.

> Make sure you have all needed dependencies, with `npm install`, before compiling the SCSS files.

There is an entry SCSS file for each layout that is located on the respective folder of the layout at `./layouts/`. These entry files are where we import all the CSS that will be used on each layout, including global CSS, additional Bootstrap modules and each component styles.

The styles for each component is located on its respective folder at `./components/` and is imported on the layout entry files.

Additionally, at the `./assets/scss/` directory we have the following folders:

- `core` - That contains global styles, typography, helper classes, Sass mixins, Bootstrap imports, animations and more.
- `fonts` - That contains the rules for importing each local font family from `./assets.fonts/`.

To avoid adding excessive unnecessary CSS to the dist files, we import from Boostrap only the modules that we use the most in the project. These modules are defined at the `core/_bootstrap.scss` file.

> If you want to use a specific module from Bootstrap that is not included by default, you can import it the entry SCSS from the page where you will need this module.

Many aspects of the Bootstrap framework are defined at the `core/_variables.scss` file, that reflects on the overall styles.

As some components and modules are needed on most pages, these are imported in the `core/_essentials.scss` file, that is included in every entry file.

For more information on writing SCSS for this theme, check the **Styles Guide** [README.md](wp-content/themes/_B_THEME_NAME/assets/scss/README.md)`.

## Localization / Translations

All translations for the front-end of the theme are placed in the `./languages` folder at the `_B_THEME_NAME` theme root, and translations for the admin area (CMS) are placed in the `./wp-content/languages/themes/` folder (from the WordPress root folder).

You can use the available **gulp** tasks, like `gulp pot` and `gulp mo`, and the [Poedit](https://poedit.net/) app, for managing the site translations.

Here is a quick overview on the related files:

- **`_B_THEME_NAME.pot`** – A template file generated using `gulp pot`. This is created by looking through all the PHP files in the theme, and getting all terms using [_gettext_](https://developer.wordpress.org/apis/handbook/internationalization/internationalization-functions/) functions.
- **`[lang].po`** – There is one file for each language/market (except English markets). These files should always be updated using the latest `_B_THEME_NAME.pot`. These files aren't read in the website, only the `.mo` files are.
- **`[lang].mo`** – These are the binary version for each `.po` file. The `.mo` files are the ones that are actually read in the website. These are updated accordingly to the content in the `.po` file when running the `gulp mo` command.

### Add new terms and translations

If you need to add new terms and their respective translations to a marke, do the following:

1. Run the `gulp pot` command to update the `_B_THEME_NAME.pot` file with all the terms (using _gettext_ functions) found in the theme php files;
2. Open the `.po` file in **Poedit**;
3. Update it accordingly to the `.pot` content, at the option **Catelog** > **Update from POT file...**;
4. Translate or update the needed items;
5. Save the updated `.po` file;
6. Run `gulp mo` to compile all `.po` files into `.mo` binary files.

### Updating existing translations

If the original text is the same and you just need to update the translation for a specific market, do the following:

1. Open the `.po` on a code editor, or using Poedit;
2. Update the text for the translations needed;
3. Save the file;
4. Run `gulp mo` to compile `.po` files into `.mo` binary files.

## More information

For more information about this project get in touch with the development team or see the documentation related to the technology your are dealing with :)
