import $ from 'jquery';
import { Component } from '@/utils/component';

class Sidebar extends Component {
	constructor() {
		super();

		// DOM reference to component's root element
		this.selfRef = $('._sidebar');
	}

	pageLoaded() {
		if (this.selfRef.length) {
		}
	}
}

export default new Sidebar();
