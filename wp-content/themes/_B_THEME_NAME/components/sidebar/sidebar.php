<?php
/**
 * Template for Theme\Components\Sidebar
 *
 * @package Theme\Components
 */

?>
<aside class="_sidebar <?php echo $class; ?>">

	<!-- PLEASE REMOVE THIS -->
	<div class="bg-medium py-9 w-100 d-flex align-items-center justify-content-center">
		<span class="h2 py-9">Sidebar</span>
	</div>

</aside>
