<?php
/**
 * Template for the Theme\Components\Analytics component
 *
 * @package Theme\Components
 */

?>
<!-- Global site tag (gtag.js) - Google Tag Manager -->
<script async src="https://www.googletagmanager.com/gtag/js?id=<?php echo $ua_code; ?>"></script>
<script>
window.dataLayer = window.dataLayer || [];

function gtag() {
	dataLayer.push(arguments);
}
gtag('js', new Date());
gtag('config', '<?php echo $ua_code; ?>');
</script>
