<?php
/**
 * Template for Theme\Components\PageHeader
 *
 * @package Theme\Components
 */

if ( ! $title ) {
	return;
}
?>
<header class="_page-header <?php echo $class; ?>">
	<div class="container">

		<!-- TITLE -->
		<h1 class="title">
			<?php echo $title; ?>
		</h1>

		<?php if ( $description ) : ?>
		<!-- DESCRIPTION -->
		<div class="description">
			<?php echo $description; ?>
		</div>
		<?php endif; ?>

	</div>

</header>
