<?php
/**
 * Template for Theme\Components\NotFoundContent
 *
 * @package Theme\Components
 */

use Theme\Components\CtaLink;

?>
<section class="_not-found-content <?php echo $class; ?>">
	<div class="container text-block py-8">

		<!-- PAGE TITLE -->
		<h1 class="title">
			<?php echo $title; ?>
		</h1>

		<!-- DESCRIPTION -->
		<p>
			<?php echo $intro; ?>
		</p>

		<!-- BUTTON -->
		<?php echo new CtaLink( array( 'link' => $cta_link ) ); ?>

	</div>
</section>
