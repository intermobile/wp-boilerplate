<?php
/**
 * Template for Theme\Components\ArticleHeader
 *
 * @package Theme\Components
 */

use Theme\Helpers\ImageObject;

?>
<div class="_article-header  <?php echo $class; ?>">

	<!-- TITLE -->
	<h1 class="title">
		<?php echo $article_data->title; ?>
	</h1>

	<div class="info">

		<!-- AUTHOR -->
		<small class="">
			<?php echo $article_data->author->name; ?>
		</small>

		<span class="">|</span>

		<!-- DATE -->
		<small class="">
			<?php echo $article_data->date; ?>
		</small>

	</div>

	<!-- FEATURED IMAGE -->
	<div class="">
		<picture class="thumbnail">
			<img
				class=""
				src="<?php echo ImageObject::get_size( $article_data->image, 'medium' )->url; ?>"
				alt="<?php echo $article_data->title; ?>" />
		</picture>
	</div>
</div>
