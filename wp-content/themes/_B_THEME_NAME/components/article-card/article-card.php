<?php
/**
 * Template for Theme\Components\ArticleCard
 *
 * @package Theme\Components
 */

use Theme\Helpers\ImageObject;

// Get data for each image size needed
$thumbnail = ImageObject::get_size( $article_data->image, 'medium' );

?>
<div class="_article-card <?php echo $class; ?>">
	<div class="card h-100">

		<!-- THUMBNAIL IMAGE -->
		<picture class="thumbnail">
			<img
				src="<?php echo $thumbnail->url; ?>"
				alt="<?php echo $article_data->title; ?>"
				width="<?php echo $thumbnail->width; ?>"
				height="<?php echo $thumbnail->height; ?>"
				loading="lazy"
				class="" />
		</picture>

		<div class="card-body">

			<!-- CATEGORY -->
			<span class="bg-primary text-white">
				<?php echo $article_data->category; ?>
			</span>

			<!-- TITLE -->
			<h3 class="card-title">
				<a href="<?php echo $article_data->permalink; ?>" class="stretched-link">
					<?php echo $article_data->title; ?>
				</a>
			</h3>

		</div>
	</div>
</div>