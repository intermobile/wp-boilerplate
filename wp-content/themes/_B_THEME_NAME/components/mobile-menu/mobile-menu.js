import $ from 'jquery';
import { Component } from '@/utils/component';
import { body } from '@/utils/globals';

class MobileMenu extends Component {
	constructor() {
		super();

		// DOM reference to component's root element
		this.selfRef = $('._mobile-menu');

		// Button that toggles the menu
		this.toggler = $('#mobile-menu-toggler');

		// Determine which the menu is currently shown or hidden
		this.isMenuOpen = false;
	}

	pageLoaded() {
		if (this.selfRef.length && this.toggler.length) {
			// Listens to the toggler click event
			this.toggler.on('click', (event) => {
				event.preventDefault();
				this.toggleMobileMenu();
			});
		}
	}

	/**
	 * Shows or hides the mobile navigation menu
	 */
	toggleMobileMenu() {
		this.isMenuOpen = !this.isMenuOpen;

		if (this.isMenuOpen) {
			// Show mobile menu
			this.selfRef.addClass('active');
			body.addClass('scroll-locked');
		} else {
			// Hide mobile menu
			this.selfRef.removeClass('active');
			body.removeClass('scroll-locked');
		}

		// Updates button attribute for accessibility
		this.toggler.attr('aria-expanded', this.isMenuOpen);
	}
}

export default new MobileMenu();
