<?php
/**
 * Template for Theme\Components\MobileMenu
 *
 * @package Theme\Components
 */

?>
<div class="_mobile-menu d-lg-none bg-dark <?php echo $class; ?>" id="_mobile-menu">
	<div class="inner-wrapper container py-6">

		<?php if ( has_nav_menu( 'mobile-menu' ) ) : ?>
		<!-- NAVIGATION -->
		<nav class="mobile-navigation">
			<?php
				wp_nav_menu(
                    array(
						'theme_location' => 'mobile-menu',
						'menu_class'     => 'menu list-unstyled',
						'container'      => false,
                    )
                );
			?>
		</nav>
		<?php else : ?>
		<p>No mobile menu found.</p>
		<?php endif; ?>

	</div>
</div>
