<?php
/**
 * Template for Theme\Components\CtaLink component
 *
 * @package Theme\Components
 */

if ( ! $link ) {
	return;
}

?>
<a href="<?php echo $link->url; ?>"
	<?php echo $link->target ? "target=\"$link->target\"" : ''; ?>
	<?php echo $link->rel ? "rel=\"$link->rel\"" : ''; ?>
	class="_cta-link <?php echo "{$btn_class} {$class}"; ?>">
	<?php echo $link->title; ?>
</a>
