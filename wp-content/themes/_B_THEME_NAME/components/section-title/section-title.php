<?php
/**
 * Template for Theme\Components\SectionTitle
 *
 * @package Theme\Components
 */

if ( ! $title ) {
	return;
}
?>
<header
	class="_section-title d-flex align-items-center justify-content-between <?php echo $class; ?>">

	<!-- TITLE -->
	<h2 class="">
		<?php echo $title; ?>
	</h2>

	<?php if ( $link_text && $link_href ) : ?>
	<!-- LINK -->
	<a class="text-nowrap" href="<?php echo esc_url( $link_href ); ?>">
		<?php echo $link_text; ?>&nbsp;⟶
	</a>
	<?php endif; ?>

</header>
