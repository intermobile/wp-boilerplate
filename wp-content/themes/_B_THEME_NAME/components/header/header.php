<?php
/**
 * Template for Theme\Components\Header
 *
 * @package Theme\Components
 */

use Theme\Components\Image;
use Theme\Components\MobileMenu;
use Theme\Helpers\ImageObject;

?>
<header class="_header bg-light <?php echo $class; ?>">
	<div class="inner-header py-3 py-lg-4">
		<div class="container d-flex justify-content-between align-items-center">

			<!-- LOGO -->
			<<?php echo $logo_tag; ?> class="site-logo mb-0">
				<a href="<?php echo home_url( '/' ); ?>" class="d-inline-flex" title="<?php echo $site_name; ?>">
					<span class="visually-hidden"><?php echo $site_name; ?></span>
					<?php
					echo new Image(
						array(
							'class'  => 'logo',
							'url'    => ImageObject::get_image_uri_from_assets( '_B_LOGO_NAME.svg' ),
							'width'  => 40,
							'height' => 40,
							'alt'    => $site_name,
						)
					);
					?>
				</a>
			</<?php echo $logo_tag; ?>><!-- .site-logo -->

			<!-- NAVIGATION MENU -->
			<?php if ( has_nav_menu( 'main-menu' ) ) : ?>
			<nav class="site-navigation d-none d-lg-block">
				<?php
					wp_nav_menu(
                        array(
							'theme_location' => 'main-menu',
							'menu_class'     => 'main-menu list-unstyled d-inline-flex mb-0',
							'container'      => false,
                        )
                    );
				?>
			</nav>
			<?php endif; ?>

			<!-- MENU BUTTON -->
			<div class="d-lg-none">
				<button id="mobile-menu-toggler" type="button" class="btn position-relative d-inline-flex"
					aria-controls="_mobile-menu"
					aria-expanded="false"
					aria-label="<?php esc_attr_e( 'Toggle navigation menu', '_B_THEME_NAME' ); ?>">
				</button>
			</div>

		</div><!-- .container -->
	</div><!-- .inner-header -->

	<!-- MOBILE MENU -->
	<?php echo new MobileMenu(); ?>

</header>
