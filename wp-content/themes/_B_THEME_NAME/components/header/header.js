import $ from 'jquery';
import { Component } from '@/utils/component';

class Header extends Component {
	constructor() {
		super();

		// DOM reference to component's root element
		this.selfRef = $('._header');
	}

	pageLoaded() {
		if (this.selfRef.length) {
		}
	}
}

export default new Header();
