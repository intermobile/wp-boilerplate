<?php
/**
 * Template for Theme\Components\LoadMoreButton
 *
 */
?>
<?php if ( $pages > 1 ) : ?>
<div class="_load-more-button<?php echo $class ? " {$class}" : ''; ?>">
	<button
		class="button btn btn-primary"
		data-nonce="<?php echo wp_create_nonce( 'load_more_posts' ); ?>"
		data-context="<?php echo $context; ?>"
		data-total-count="<?php echo $count; ?>"
		data-container="<?php echo $container; ?>"
		data-url="<?php echo site_url() . '/wp-admin/admin-ajax.php'; ?>"
		data-value="<?php echo $value; ?>"
		data-search="<?php echo $search; ?>"
		data-pages="<?php echo $pages; ?>"
		data-exclude="<?php echo json_encode( ( isset( $exclude ) ) ? $exclude : array() ); ?>"
		data-item=".col">
		<!-- <span class="spinner spinner-border spinner-border-sm d-none mr-2" role="status" aria-hidden="true"></span> -->
		<div class="lds-ring hide-spinner ">
			<div></div>
			<div></div>
			<div></div>
			<div></div>
		</div>
		<span class="text">
			<?php echo $button_text; ?>
		</span>
	</button>
</div>
<?php endif; ?>
