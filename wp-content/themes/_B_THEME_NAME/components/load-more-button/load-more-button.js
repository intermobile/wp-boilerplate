import $ from 'jquery';
import axios from 'axios';
import qs from 'qs';
import { Component } from '../../assets/js/utils/component';
let page = 1;

class LoadMoreButton extends Component {
	constructor() {
		super();

		// DOM reference to component's root element
		this.selfRef = $('._load-more-button');
	}

	pageLoaded() {
		if (this.selfRef.length) {
			// Iterate though each instance of this component
			this.selfRef.each((index, element) => {
				const button = $(element).find('.button');

				// Register the handler event
				button.on('click', this.handleLoadMorePosts);
			});
		}
	}

	// Handles the logic for loading more posts
	handleLoadMorePosts(event) {
		const button = $(event.currentTarget);
		page += 1;

		const data = {
			action: 'load_more_posts',
			context: button.data('context'),
			number: button.data('total-count'),
			value: button.data('value'),
			search: button.data('search'),
			exclude: button.data('exclude'),
			page: page,
			_wpnonce: button.data('nonce'),
		};

		const options = {
			method: 'POST',
			headers: { 'content-type': 'application/x-www-form-urlencoded' },
			data: qs.stringify(data),
			url: button.data('url'),
		};

		// Update button
		button.prop('disabled', true);
		button.find('span').addClass('d-none');
		button.find('.lds-ring').addClass('spinner-border spinner-border-sm');
		//button.find('.lds-ring').removeClass('hide-spinner');

		// Make the request for more posts
		axios(options)
			.then(function (response) {
				console.log('response: ' + JSON.stringify(response));
				if (response) {
					console.log('response: ' + response);
					$(button.data('container')).append(response.data.html);
					button.data('exclude', response.data.exclude);

					// Hides button if there is no more content to load
					if (page === button.data('pages')) {
						button.hide();
					}
				} else {
					console.log('No Response');
				}

				// Restore button
				button.prop('disabled', false);
				button.find('span').removeClass('d-none');
				button.find('.lds-ring').removeClass('spinner-border spinner-border-sm');
				//button.find('.lds-ring').addClass('hide-spinner');
				button.trigger('focus');
			})
			.catch(function (error) {
				// Restore button
				button.prop('disabled', false);
				button.find('span').removeClass('d-none');
				button.find('.lds-ring').removeClass('spinner-border spinner-border-sm');
				//button.find('.lds-ring').addClass('hide-spinner');
				button.trigger('focus');
			});
	}
}

export default new LoadMoreButton();
