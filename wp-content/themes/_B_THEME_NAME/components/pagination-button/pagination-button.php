<?php
/**
 * Template for Theme\Components\PaginationButton
 *
 */

$paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
$links = array();

if ( wp_is_mobile() ) {
	$aux_paged = 2;
	$aux_max = 3;
	$aux_paged_decrement_increment = 1;
} else {
	$aux_paged = 3;
	$aux_max = 5;
	$aux_paged_decrement_increment = 2;
}

$page_init = $paged <= $aux_paged ? 1 : $max - $aux_paged_decrement_increment;
if ( $paged <= $aux_paged || $max <= 5 ) {
	$page_init = 1;
} else {
	$page_init = $paged - $aux_paged_decrement_increment;
}

if ( $paged >= $aux_paged ) {
	$max_page = ( ( $max - $paged ) > 2 ) ? ( $paged + $aux_paged_decrement_increment ) : $max;
} else {
	$max_page = $max < $aux_max ? $max : $aux_max;
}

// Add page number links
for ( $i = $page_init; $i <= $max_page; $i++ ) {
	array_push($links, $i);
}

?>
<div class="_pagination-button <?php echo $class; ?>">
    <div class="nav-links">
        <a class="<?php echo $paged == 1 ? 'disable' : ''; ?>"
            href="<?php echo esc_url( get_pagenum_link( 1 ) ); ?>"
            data-tracking='link-click'><?php echo __( 'First', '' ); ?></a>
        <a class="page-numbers <?php echo $paged == 1 ? 'disable' : ''; ?>"
            href="<?php echo esc_url( get_pagenum_link( $paged - 1 ) ); ?>" data-tracking='link-click'>
            <
                </a>
                <?php
				foreach ( $links as $key => $link ) :
					$class = $link == $paged ? ' active' : '';
					?>
                <a class="page-numbers<?php echo $class; ?>"
                    href="<?php echo esc_url( get_pagenum_link( $link ) ); ?>"
                    data-tracking='link-click'><?php echo $link; ?></a>
					<?php
				endforeach;

				?>
                <a class="page-numbers <?php echo $max_page != $paged ? '' : 'disable'; ?>"
                    href="<?php echo esc_url( get_pagenum_link( $paged + 1 ) ); ?>" data-tracking='link-click'> > </a>
                <a class="<?php echo $max_page != $paged ? '' : 'disable'; ?>"
                    href="<?php echo esc_url( get_pagenum_link( $max ) ); ?>"
                    data-tracking='link-click'><?php echo __( 'Last', '' ); ?></a>
    </div>
</div>
