<?php
/**
 * Template for Theme\Components\Image
 *
 * @package Theme\Components
 */

use Theme\Components\Image;

?>
<img
	loading="lazy"
	class="_image <?php echo $class; ?>"
	src="<?php echo esc_url( $url ); ?>"
	width="<?php echo $width; ?>"
	height="<?php echo $height; ?>"
	alt="<?php echo esc_attr( $alt ); ?>"
	<?php echo Image::get_attributes_string( $attr ); ?> />