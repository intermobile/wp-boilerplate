<?php
/**
 * Template for Theme\Components\ArticlesList
 *
 * @package Theme\Components
 */

use Theme\Components\ArticleCard;
use Theme\Components\SectionTitle;

?>
<section class="_articles-list <?php echo $class; ?>">
	<div class="container">

		<?php
		// SECTION TITLE
		echo new SectionTitle(
			array(
				'title'     => $title,
				'link_text' => $link_text,
				'link_href' => $link_href,
			)
		);
		?>

		<!-- CARDS LIST -->
		<div class="row">
			<?php foreach ( $articles as $key => $article_data ) : ?>
			<div class="col-12 col-md-4 mb-5">
				<?php
                echo new ArticleCard(
                    array(
						'class'        => 'h-100',
						'article_data' => $article_data,
					)
                );
				?>
			</div>
			<?php endforeach; ?>
		</div>

	</div>
</section>
