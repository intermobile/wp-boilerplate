<?php
/**
 * Template for Theme\Components\ContainerGuide component
 *
 * @package Theme\Components
 */

?>
<div class="_container-guide w-100 h-100 fixed-top <?php echo $class; ?>">
	<div class="container">
		<div class="guide"></div>
	</div>
</div>
