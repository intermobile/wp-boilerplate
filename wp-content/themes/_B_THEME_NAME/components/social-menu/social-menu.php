<?php
/**
 * Template for Theme\Components\SocialMenu
 *
 * @package Theme\Components
 */

if ( ! has_nav_menu( $theme_location ) ) {
	return;
}
?>
<nav class="_social-menu <?php echo $class; ?>">

	<!-- TITLE -->
	<h3 class="visually-hidden">
		<?php echo $title; ?>
	</h3>

	<!-- MENU -->
	<?php
	wp_nav_menu(
        array(
			'theme_location' => $theme_location,
			'menu_class'     => "d-inline-flex list-unstyled mb-0 {$menu_class}",
			'container'      => false,
			'depth'          => 1,
        )
    );
	?>
</nav>
