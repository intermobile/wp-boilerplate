<?php
/**
 * Template for Theme\Components\RichContent
 *
 * @package Theme\Components
 */

?>
<div class="_rich-content <?php echo $class; ?>">
	<?php echo $content; ?>
</div>
