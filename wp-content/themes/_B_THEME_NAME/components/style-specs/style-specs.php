<?php
/**
 * Template for Theme\Components\StyleSpecs component
 *
 * This component also imports Vue.js and its own scripts, so that it keeps independent and doesn't need to be managed on bundles.
 *
 * @package Theme\Components
 */

?>
<div class="_style-specs<?php echo isset( $class ) ? " {$class}" : ''; ?>">
	<div class="container">
		<!-- TITLE & DESCRIPTION -->
		<header class="pt-4 pb-2">
			<div class="row">
				<div class="col012 col-md-4">
					<h1>Style Specs</h1>
				</div>
				<div class="col-12 col-md-8">
					<p>
						Check below the core styles for this theme.</a>
					</p>
					<p>
						<small>
							All components below are configured in the <code>_variables.scss</code> file,
							and complementarly on other core SCSS files.
						</small><br>
						<small>
							Definitions are collected from the <code>theme.dist.css</code>, so make sure it is updated.
						</small>
					</p>
				</div>
			</div>
			<p class="text-danger" v-if="!isSupported">
				<strong
					v-html="'⚠️ This browser doesn\'t seems to support some features that this component needs, so some specs below may not work properly.'"></strong>
			</p>
		</header>

		<hr>

		<!-- HEADINGS -->
		<section class="row pb-3 pt-4">
			<div class="col-12 col-md-4">
				<h2>Typography</h2>
				<p class="text-muted">Font size, weight and family.</p>
				<p>
					<small class="text-info font-weight-bold">
						Resize the viewport to see the specs <br>
						for different resolutions (<code class="text-muted">&lt;992px</code>).
					</small>
				</p>
			</div>
			<div class="col-12 col-md-8">

				<!-- H1 -->
				<div class="row mb-4">
					<div class="col-6 d-flex align-items-center">
						<h1 class="text-spec-element mt-0 mb-0">Heading 1</h1>
					</div>
					<div class="col-6 text-muted d-flex align-items-center">
						<p class="mb-0" v-if="typeof typography[0] !== 'undefined'">
							<code v-html="typography[0].size">Size</code>
							-
							<small>
								<span v-html="typography[0].family">Family</span>,
								<span v-html="typography[0].weight">Weight</span>
							</small>
						</p>
					</div>
				</div>

				<!-- H2 -->
				<div class="row mb-4">
					<div class="col-6 d-flex align-items-center">
						<h2 class="text-spec-element mt-0 mb-0">Heading 2</h2>
					</div>
					<div class="col-6 text-muted d-flex align-items-center">
						<p class="mb-0" v-if="typeof typography[1] !== 'undefined'">
							<code v-html="typography[1].size">Size</code>
							-
							<small>
								<span v-html="typography[1].family">Family</span>,
								<span v-html="typography[1].weight">Weight</span>
							</small>
						</p>
					</div>
				</div>

				<!-- H3 -->
				<div class="row mb-4">
					<div class="col-6 d-flex align-items-center">
						<h3 class="text-spec-element mt-0 mb-0">Heading 3</h3>
					</div>
					<div class="col-6 text-muted d-flex align-items-center">
						<p class="mb-0" v-if="typeof typography[2] !== 'undefined'">
							<code v-html="typography[2].size">Size</code>
							-
							<small>
								<span v-html="typography[2].family">Family</span>,
								<span v-html="typography[2].weight">Weight</span>
							</small>
						</p>
					</div>
				</div>

				<!-- H4 -->
				<div class="row mb-4">
					<div class="col-6 d-flex align-items-center">
						<h4 class="text-spec-element mt-0 mb-0">Heading 4</h4>
					</div>
					<div class="col-6 text-muted d-flex align-items-center">
						<p class="mb-0" v-if="typeof typography[3] !== 'undefined'">
							<code v-html="typography[3].size">Size</code>
							-
							<small>
								<span v-html="typography[3].family">Family</span>,
								<span v-html="typography[3].weight">Weight</span>
							</small>
						</p>
					</div>
				</div>

				<!-- H5 -->
				<div class="row mb-4">
					<div class="col-6 d-flex align-items-center">
						<h5 class="text-spec-element mt-0 mb-0">Heading 5</h5>
					</div>
					<div class="col-6 text-muted d-flex align-items-center">
						<p class="mb-0" v-if="typeof typography[4] !== 'undefined'">
							<code v-html="typography[4].size">Size</code>
							-
							<small>
								<span v-html="typography[4].family">Family</span>,
								<span v-html="typography[4].weight">Weight</span>
							</small>
						</p>
					</div>
				</div>

				<!-- H6 -->
				<div class="row mb-4">
					<div class="col-6 d-flex align-items-center">
						<h6 class="text-spec-element mt-0 mb-0">Heading 6</h6>
					</div>
					<div class="col-6 text-muted d-flex align-items-center">
						<p class="mb-0" v-if="typeof typography[5] !== 'undefined'">
							<code v-html="typography[5].size">Size</code>
							-
							<small>
								<span v-html="typography[5].family">Family</span>,
								<span v-html="typography[5].weight">Weight</span>
							</small>
						</p>
					</div>
				</div>

				<hr>

				<!-- LEAD TEXT -->
				<div class="row mb-4">
					<div class="col-6 d-flex align-items-center">
						<p class="text-spec-element lead mt-0 mb-0">Lead text</p>
					</div>
					<div class="col-6 text-muted d-flex align-items-center">
						<p class="mb-0" v-if="typeof typography[6] !== 'undefined'">
							<code v-html="typography[6].size">Size</code>
							-
							<small>
								<span v-html="typography[6].family">Family</span>,
								<span v-html="typography[6].weight">Weight</span>
							</small>
						</p>
					</div>
				</div>

				<!-- BODY -->
				<div class="row mb-4">
					<div class="col-6 d-flex align-items-center">
						<p class="text-spec-element mt-0 mb-0">Body text</p>
					</div>
					<div class="col-6 text-muted d-flex align-items-center">
						<p class="mb-0" v-if="typeof typography[7] !== 'undefined'">
							<code v-html="typography[7].size">Size</code>
							-
							<small>
								<span v-html="typography[7].family">Family</span>,
								<span v-html="typography[7].weight">Weight</span>
							</small>
						</p>
					</div>
				</div>

				<!-- BODY (SMALL) -->
				<div class="row mb-4">
					<div class="col-6 d-flex align-items-center">
						<small class="text-spec-element mt-0 mb-0">Small text</small>
					</div>
					<div class="col-6 text-muted d-flex align-items-center">
						<p class="mb-0" v-if="typeof typography[8] !== 'undefined'">
							<code v-html="typography[8].size">Size</code>
							-
							<small>
								<span v-html="typography[8].family">Family</span>,
								<span v-html="typography[8].weight">Weight</span>
							</small>
						</p>
					</div>
				</div>

			</div>
		</section>

		<hr>

		<!-- COLORS -->
		<section class="row pb-3 pt-4">
			<div class="col-12 col-md-4">
				<h2>Colors</h2>
				<p class="text-muted">Brand and theme colors.</p>
				<p>
					<small class="text-info font-weight-bold">
						These colors are available for classes like <br>
						<code class="text-muted">.bg-color</code>,
						<code class="text-muted">.text-color</code> and <code class="text-muted">.btn-color</code>.
					</small>
				</p>
			</div>
			<div class="col-12 col-md-8">
				<div class="row mb-6">
					<div v-for="color in colors" :key="color.class" class="col-4 col-md-3 col-lg-2 text-center mb-4">
						<div :class="[color.class, color.name == 'white' ? 'border' : '']"
							class="color p-6 rounded-circle d-inline-block"></div>
						<div>
							<code class="d-block mb-1" v-html="color.name">Name</code>
							<code><small class="text-muted text-uppercase" v-html="color.value">Value</small></code>
						</div>
					</div>
				</div>
			</div>
		</section>

		<hr>

		<!-- ICONS -->
		<section class="row pt-4">
			<div class="col-12 col-md-4">
				<h2>Icons</h2>
				<p class="text-muted">Available from the icons font family.</p>
				<p>
					<small class="text-info font-weight-bold">
						Available using the proper <code class="text-muted">.icon-name</code> class.<br>
						Also via SCSS, with the <code class="text-muted">icon(name)</code> mixin.
					</small>
				</p>
			</div>
			<div class="col-12 col-md-8">
				<div class="row mb-6">
					<div v-for="icon in icons" :key="icon.class" class="col-4 col-md-3 col-lg-2 text-center mb-4">
						<i :class="[icon.class]" class="h1 d-block mb-0"></i>
						<code class="d-block text-muted" v-html="icon.name">Name</code>
					</div>
				</div>
			</div>
		</section>

		<hr>

		<!-- SPACING -->
		<section class="row pb-3 pt-4">
			<div class="col-12 col-md-4">
				<h2>Spacing</h2>
				<p class="text-muted">Margin & Padding sizes.</p>
				<p>
					<small class="text-info font-weight-bold">
						Available sizes for margin and padding classes,<br>
						like <code class="text-muted">.m-1</code>, <code class="text-muted">.mx-sm-n3</code>,
						<code class="text-muted">.pt-4</code>, <code class="text-muted">.py-lg-6</code>.
					</small>
				</p>
			</div>
			<div class="col-12 col-md-8">
				<div class="row mb-6">
					<div v-for="spacing in spacings" :key="spacing.number"
						class="col-6 col-sm-4 col-lg text-center d-inline-flex flex-column mb-4">
						<div>
							<code class="d-block h4 mb-1" v-html="spacing.number">Name</code>
							<code><small class="text-muted d-inline-block d-lg-block">
								<small v-html="`${spacing.rem}rem`">Value</small>
							</small></code>
							<code><small class="text-muted" v-html="`${spacing.px}px`">Value</small></code>
						</div>
						<div class="spacing d-inline-block bg-primary px-2 mt-2" :class="['pt-' + spacing.number]">
						</div>
					</div>
				</div>
			</div>
		</section>

		<hr>

		<!-- BUTTONS -->
		<section class="row pb-2 pt-4">
			<div class="col-12 col-md-4">
				<h2>Buttons</h2>
				<p class="text-muted">Available button variations.</p>
			</div>
			<div class="col-12 col-md-8">

				<!-- BUTTON (DEFAULT) -->
				<div class="row py-4 rounded">
					<div class="col-5 d-flex flex-column">
						<div class="button-wrapper mb-3">
							<a href="#" class="btn btn-primary me-2 btn-lg button-spec-element"
								onclick="return false">
								Button Large
							</a>
						</div>
					</div>
					<div class="col-7 text-muted d-flex align-items-center"
						v-if="typeof buttons[0] !== 'undefined'">
						<code>{{ buttons[0].size }},↕{{ buttons[0].height }}px</code>&nbsp;-&nbsp;
						<small>{{ buttons[0].family }}, {{ buttons[0].weight }}</small>
					</div>
					<div class="col-5 d-flex flex-column">
						<div class="button-wrapper mb-3">
							<a href="#" class="btn btn-primary me-2 button-spec-element"
								onclick="return false">
								Button Default
							</a>
						</div>
					</div>
					<div class="col-7 text-muted d-flex align-items-center"
						v-if="typeof buttons[1] !== 'undefined'">
						<code>{{ buttons[1].size }},↕{{ buttons[1].height }}px</code>&nbsp;-&nbsp;
						<small>{{ buttons[1].family }}, {{ buttons[1].weight }}</small>
					</div>
					<div class="col-5 d-flex flex-column">
						<div class="button-wrapper">
							<a href="#" class="btn btn-primary me-2 btn-sm button-spec-element"
								onclick="return false">
								Button Small
							</a>
						</div>
					</div>
					<div class="col-7 text-muted d-flex align-items-center"
						v-if="typeof buttons[2] !== 'undefined'">
						<code>{{ buttons[2].size }},↕{{ buttons[2].height }}px</code>&nbsp;-&nbsp;
						<small>{{ buttons[2].family }}, {{ buttons[2].weight }}</small>
					</div>
				</div>

				<!-- BUTTON (WHITE) -->
				<div class="row bg-primary py-4 mb-6 rounded">
					<div class="col-5 d-flex flex-column">
						<div class="button-wrapper mb-3">
							<a href="#" class="btn btn-white me-2 btn-lg button-spec-element"
								onclick="return false">
								Button Large
							</a>
						</div>
					</div>
					<div class="col-7 text-white d-flex align-items-center"
						v-if="typeof buttons[3] !== 'undefined'">
						<code class="text-white">{{ buttons[3].size }},↕{{ buttons[3].height }}px</code>&nbsp;-&nbsp;
						<small>{{ buttons[3].family }}, {{ buttons[3].weight }}</small>
					</div>
					<div class="col-5 d-flex flex-column">
						<div class="button-wrapper mb-3">
							<a href="#" class="btn btn-white me-2 button-spec-element"
								onclick="return false">
								Button Default
							</a>
						</div>
					</div>
					<div class="col-7 text-white d-flex align-items-center"
						v-if="typeof buttons[4] !== 'undefined'">
						<code class="text-white">{{ buttons[4].size }},↕{{ buttons[4].height }}px</code>&nbsp;-&nbsp;
						<small>{{ buttons[4].family }}, {{ buttons[4].weight }}</small>
					</div>
					<div class="col-5 d-flex flex-column">
						<div class="button-wrapper">
							<a href="#" class="btn btn-white me-2 btn-sm button-spec-element"
								onclick="return false">
								Button Small
							</a>
						</div>
					</div>
					<div class="col-7 text-white d-flex align-items-center"
						v-if="typeof buttons[5] !== 'undefined'">
						<code class="text-white">{{ buttons[5].size }},↕{{ buttons[5].height }}px</code>&nbsp;-&nbsp;
						<small>{{ buttons[5].family }}, {{ buttons[5].weight }}</small>
					</div>
				</div>

		</section>
		<input type="hidden" id="theme-styles-uri" data-src="<?php echo $css_uri; ?>">
	</div>
</div>