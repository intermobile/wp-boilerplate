/**
 * This is an independent script to be used only by the Style Specs components
 * It is loaded when the component is imported, so there is no need to bundle it on a dist file.
 */

// Create a Vue instance for the component
var styleSpecs = new Vue({
	el: '._style-specs',
	data: {
		typography: [],
		colors: [],
		icons: [],
		spacings: [],
		buttons: [],
		styles: null,
		isSupported: true,
	},
	methods: {
		updateTypographySpecs: function () {
			const textElements = document.querySelectorAll('._style-specs .text-spec-element');
			if (
				this.typography.length &&
				window.getComputedStyle(textElements[0]).fontSize === this.typography[0].size
			) {
				return;
			} else {
				this.typography = [];
				textElements.forEach((headingEl, i) => {
					this.typography.push({
						size: window.getComputedStyle(headingEl).fontSize,
						family: window.getComputedStyle(headingEl).fontFamily.split(',')[0].replace(/["']/g, '').trim(),
						weight: window.getComputedStyle(headingEl).fontWeight,
					});
				});
			}
		},
		updateButtonsSpecs: function () {
			document.querySelectorAll('._style-specs .button-spec-element').forEach((buttonEl, i) => {
				this.buttons.push({
					size: window.getComputedStyle(buttonEl).fontSize,
					height: Math.round(parseInt(window.getComputedStyle(buttonEl).height.replace('px', ''))),
					family: window.getComputedStyle(buttonEl).fontFamily.split(',')[0].replace(/["']/g, '').trim(),
					weight: window.getComputedStyle(buttonEl).fontWeight,
				});
			});
		},
		buildColorsList: function () {
			// Get all occurences for bg clases
			const allClasses = this.styles.match(/(?<=\.bg-)((\w+[-_]?)+)/g);

			// Remove irrelevant colors
			const filteredClasses = allClasses.filter(
				(item) => !item.match(/^(body)|(transparent)|(opacity-\d+)|(gradient)$/g)
			);

			// Get color from each class
			filteredClasses.forEach((name) => {
				const pattern = new RegExp(`(?<=--bs-${'primary'}: )#[^;]+`);
				const color = this.styles.match(pattern)[0];
				styleSpecs.colors.push({
					class: `bg-${name}`,
					name: name,
					value: color,
				});
			});
		},
		buildSpacingsList: function () {
			// Get all occurences for bg clases
			const matches = this.styles.match(/\.p-md-(\d+)\s*{[\n\s]*padding:\s*(\d*\.?\d*?rem)/g);

			// Update the list
			matches.forEach((spacing) => {
				styleSpecs.spacings.push({
					number: spacing.match(/(\d+)(?=\s?{)/)[0],
					rem: spacing.match(/(\d*\.?\d*)(?=rem)/)[0],
					px: parseFloat(spacing.match(/(\d*\.?\d*)(?=rem)/)[0]) * 16,
				});
			});
		},
		buildIconsList: function () {
			// Get all occurences for bg clases
			const matches = styleSpecs.styles.match(/(?<=\.icon-)[\w-]+(?=:before)/g);

			// Remove duplicated occurences
			const classes = matches.filter((item, i) => matches.indexOf(item) === i);

			// Update the list
			classes.forEach((icon) => {
				styleSpecs.icons.push({
					class: `icon-${icon}`,
					name: icon,
				});
			});
		},
	},
	created: function () {
		// Set specs for headings, body text typography and buttons
		this.updateTypographySpecs();
		this.updateButtonsSpecs();

		// Fetch content from the main CSS file.
		const stylesUri = document.getElementById('theme-styles-uri').getAttribute('data-src');
		fetch(stylesUri)
			.then((response) => {
				return response.text();
			})
			.then((data) => {
				styleSpecs.styles = data;

				// Set specs for the theme colors and icons
				styleSpecs.buildColorsList();
				styleSpecs.buildIconsList();
				styleSpecs.buildSpacingsList();
			});
	},
});

// Bind to window resize event
window.addEventListener('resize', function () {
	// Update headings and body text typography specs accordingly to screen size
	styleSpecs.updateTypographySpecs();
});
