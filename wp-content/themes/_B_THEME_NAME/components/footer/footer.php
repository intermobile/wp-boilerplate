<?php
/**
 * Template for Theme\Components\Footer
 *
 * @package Theme\Components
 */

use Theme\Components\Image;
use Theme\Components\SocialMenu;
use Theme\Helpers\ImageObject;

?>
<footer class="_footer bg-light <?php echo $class; ?>">
	<div class="inner-footer container py-5">

		<!-- TITLE (FOR ACESSIBILITY) -->
		<h2 class="visually-hidden"><?php esc_html_e( 'Site Footer', '_B_THEME_NAME' ); ?></h2>

		<div class="row">

			<!-- LOGO -->
			<div class="col-12 col-md-3">
				<a href="<?php echo get_home_url( '/' ); ?>" aria-label="<?php bloginfo( 'name' ); ?>"
					class="d-inline-block">
					<?php
					echo new Image(
						array(
							'class'  => 'logo',
							'url'    => ImageObject::get_image_uri_from_assets( '_B_LOGO_NAME.svg' ),
							'width'  => 32,
							'height' => 32,
							'alt'    => get_bloginfo( 'name' ),
						)
					);
					?>
				</a>
			</div>

			<!-- FOOTER LINKS -->
			<div class="col-12 col-md-6 d-flex justify-content-center">
				<?php if ( has_nav_menu( 'footer-menu' ) ) : ?>
				<nav class="footer-menu">
					<?php
                    wp_nav_menu(
                        array(
							'theme_location' => 'footer-menu',
							'menu_class'     => 'list-unstyled d-flex text-whtie',
							'container'      => false,
                        )
                    );
                    ?>
				</nav>
				<?php else : ?>
				No menu found.
				<?php endif; ?>
			</div>

			<!-- SOCIAL MENU -->
			<div class="col-12 col-md-3">
				<?php
				echo new SocialMenu(
					array(
						'title'          => esc_html__( 'Follow us on social media', 'captalysasset' ),
						'theme_location' => 'social-menu',
					)
				);
				?>
			</div>

		</div>
	</div>

	<!-- COPYRIGHT -->
	<div class="container">
		<p class="copyright text-center">
			<?php echo $copyright_text; ?>
		</p>
	</div>
</footer>
