'use strict';

var colors = require('colors'); // Enable the use of colors for logs in console
console.log(colors.gray('Reading gulpfile.js...'));

// Required modules
var gulp = require('gulp'), // Gulp tasks
	pump = require('pump'), // Replace pipe() with pump for a better error handling
	path = require('path'), // Node.js module for handling paths
	ora = require('ora'), // Updated log messages (loading spinners)
	fs = require('fs'); // Node.js module for managing files and directories (used to get package.json)

const gulpFolder = './tools/gulp';

// Content of the theme.json file
// Where is defined the project info, directories, files and more
const gulpConfig = JSON.parse(fs.readFileSync(`${gulpFolder}/config.json`));

// Spinner used for loading messages
var spinner = null;

// ----------------------------------- UTILS ----------------------------------- //
// ----------------------------------------------------------------------------- //

/** Error handler to be used on error events */
function errorLog(err) {
	if (spinner) {
		spinner.fail(colors.red(spinner.text));
		clearSpinner();
	}
	console.log.bind(err);
}

/** Create and starts a new spinner with the given text */
function startSpinner(message) {
	if (!ora) {
		ora = require('ora');
	}
	spinner = ora({
		text: message,
		color: 'green',
	});
	spinner.start();
}

/** Reset the spinner class */
function clearSpinner() {
	spinner = null;
}

/** Fetch command line arguments */
const arg = ((argList) => {
	let arg = {},
		a,
		opt,
		thisOpt,
		curOpt;
	for (a = 0; a < argList.length; a++) {
		thisOpt = argList[a].trim();
		opt = thisOpt.replace(/^\-+/, '');

		if (opt === thisOpt) {
			// argument value
			if (curOpt) arg[curOpt] = opt;
			curOpt = null;
		} else {
			// argument name
			curOpt = opt;
			arg[curOpt] = true;
		}
	}
	return arg;
})(process.argv);

// -------------------------------- CORE TASKS -------------------------------- //
// ---------------------------------------------------------------------------- //

/**
 * STYLES TASK
 * Compile the SCSS files (including Bootstrap, if imported) into the theme style.css
 */
var watchingStyles = false;
var scssEntries = null;
function sass(done) {
	// Import modules
	const rename = require('gulp-rename');
	const prefix = require('gulp-autoprefixer');
	const minimatch = require('minimatch');
	const gulpSass = require('gulp-sass')(require('sass'));
	const sourcemaps = require('gulp-sourcemaps');

	// Get settings and args
	const config = gulpConfig.tasks.sass;
	const watch = arg.watch || arg.w || false;
	const devMode = arg.development || arg.dev || arg.d || false;

	// Get list with all entries
	if (!watchingStyles) {
		scssEntries = config.entry;
	}

	// Check for the watch flag
	if (watch) {
		if (!watchingStyles) {
			gulp.watch(config.files, gulp.series(sass)).on('change', (changedFile) => {
				scssEntries = [];
				//console.log('changed', changedFile);
				config.entry.forEach((entry) => {
					if (minimatch(changedFile, entry)) {
						// Add the own file to the list, removing underscore on name
						scssEntries.push(changedFile.replace(/\/_(?=[\w-]+\.scss)/, '/'));
					}
				});
				// Add the full list if the changed file is not an entry file
				if (scssEntries.length === 0) {
					scssEntries = config.entry;
				}
			});
			watchingStyles = true;
		}
	}

	//console.log('Compiling:', scssEntries.join(', '));

	// Prepare success info message
	const successInfo =
		scssEntries.length === 1
			? ` - CSS file generated for ${scssEntries[0]}`
			: ` - CSS files generated at ${config.dist}`;

	// Complile the SASS files
	if (devMode) {
		return pump(
			[
				gulp.src(scssEntries).on('data', () => {
					if (!spinner) startSpinner('Compiling Sass');
				}),
				sourcemaps.init(),
				gulpSass({
					outputStyle: 'expanded',
					includePaths: config.paths,
				}).on('error', errorLog),
				prefix('last 4 versions'),
				rename(function (path, file) {
					path.dirname = getDistFolder(file.path);
					path.basename += '.dist';
				}),
				sourcemaps.write('./').on('end', () => {
					spinner.succeed(colors.green(spinner.text) + colors.gray(successInfo));
					clearSpinner();
				}),
				gulp.dest(config.dist).on('end', sassCompleted),
			],
			done
		);
	} else {
		return pump(
			[
				gulp.src(scssEntries).on('data', () => {
					if (!spinner) startSpinner('Compiling Sass');
				}),
				gulpSass({
					outputStyle: 'compressed',
					includePaths: config.paths,
				}).on('error', errorLog),
				prefix('last 4 versions').on('end', () => {
					spinner.succeed(colors.green(spinner.text) + colors.gray(successInfo));
					clearSpinner();
				}),
				rename(function (path, file) {
					path.dirname = getDistFolder(file.path);
					path.basename += '.dist';
				}),
				gulp.dest(config.dist).on('end', sassCompleted),
			],
			done
		);
	}

	// Get the respective parent folder for the dist file
	function getDistFolder(path = '') {
		// Remove file and folder with same name
		const base = path.replace(/(?:[\/\\]([\w-]+)?[\/\\](\1)\.\w+$)|(?:[\/\\][\w-]+\.\w+$)/, '');
		// Gets the parent folder name
		const match = base.match(/\w+$/);

		const folder = match ? match[0] : '';
		return `./${folder}`;
	}

	function sassCompleted() {
		if (watchingStyles)
			console.log(colors.cyan('Watching ' + (watchingScripts ? 'js and ' : '') + 'scss files for changes...'));
		else done();
	}
}

/**
 * JAVASCRIPT TASK
 * Compile and bundles all JS files
 */
var watchingScripts = false;
function js(done) {
	// Import modules
	const webpackConfig = require('./webpack.config');
	const webpack = require('piped-webpack');
	const gutil = require('gulp-util');

	// Get settings and args
	const config = gulpConfig.tasks.js;
	const page = arg.page || arg.p || null;
	const watch = arg.watch || arg.w || false;
	const devMode = arg.dev || arg.development || arg.d || false;

	// Check if a specific page was specified
	if (page) {
		config.entry = config.entry.replace('/**/*.js', `/${page}/${page}.js`);

		// Check for entry file existence
		if (!fs.existsSync(path.resolve(__dirname, config.entry))) {
			console.log(colors.red('Problem: The entry file (' + config.entry + ") wasn't found."));
			done();
			return;
		}
	}

	// Check for the watch flag to watch JS files
	if (watch) {
		if (!watchingScripts) {
			gulp.watch(config.files, gulp.series(js)).on('error', gutil.log);
			watchingScripts = true;
		}
	}

	// Check for development flag
	if (devMode) {
		webpackConfig.devtool = 'cheap-eval-source-map';
		webpackConfig.mode = 'development';
	}

	// Compile the script files
	return pump(
		[
			gulp.src(config.entry).on('data', () => {
				if (!spinner) {
					//startSpinner('Compiling Scripts');
				}
			}),
			webpack(webpackConfig)
				.on('error', errorLog)
				.on('end', () => {
					//spinner.succeed(colors.green(spinner.text));
					//clearSpinner();
				}),
			gulp.dest(config.dist).on('end', function (event) {
				console.log(
					colors.green('✔ JS files compiled successfully at ' + config.dist) +
						(devMode ? colors.yellow(' [development mode]') : colors.gray(' [production mode]'))
				);
				// Check for the watch flag
				if (watchingScripts) {
					console.log(
						colors.cyan('Watching js ' + (watchingStyles ? 'and scss ' : '') + 'files for changes...')
					);
				} else {
					done();
				}
			}),
		],
		done
	);
}

// ------------------------------ AVAILABLE TASKS ----------------------------- //
// ---------------------------------------------------------------------------- //

/**
 * Imports the module of the required task passing the proper data to the script file
 * @param {string} task Name of the task and respective JS file
 */
function getTask(task) {
	const helpers = { pump, colors, ora, fs, path };
	gulpConfig.project.path = __dirname;
	return require(`${gulpFolder}/tasks/` + task)(gulp, gulpConfig.tasks[task], gulpConfig.project, helpers, arg);
}

// Register all tasks from the tasks folder
fs.readdirSync(`${gulpFolder}/tasks/`).forEach((file) => {
	const task = file.replace(/\.\w+$/, ''); // Remove extension
	exports[task] = getTask(task);
});

exports.js = js;
exports.sass = sass;

/** Default task - Compiles styles and scripts files */
gulp.task('default', gulp.series(sass, js));
