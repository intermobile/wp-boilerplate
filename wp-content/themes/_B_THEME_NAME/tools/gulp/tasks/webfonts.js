/**
 * A Gulp task for converting font files to different formats
 * @param {*} gulp Instance of gulp
 * @param {object} config Tasks configurations
 * @param {object} project Project info
 * @param {object} hp Additional helper functions already imported on the gulpfile
 * @param {object} arg Object with arguments passed via command line
 */
module.exports = function (gulp, config, project, hp, arg) {
	return function () {
		// Import modules
		const ttf2woff2 = require('gulp-ttf2woff2');
		const ttf2woff = require('gulp-ttf2woff');

		return new Promise(async (done, reject) => {
			let family, srcPath, woffDist, woff2Dist;

			// Check if the family/folder name was specified
			if (!arg.family && !arg.F) {
				reject(
					new Error(
						hp.colors.red(
							`Please inform the name of the folder of the font family to convert, in the ${hp.colors.cyan(
								'--family'
							)} (or ${hp.colors.cyan('-F')}) flag.`
						)
					)
				);
			} else {
				// Set folder name and path
				family = arg.F || arg.family;
				srcPath = config.entry.replace('{family}', family);
				woffDist = config.woffDist.replace('{family}', family);
				woff2Dist = config.woff2Dist.replace('{family}', family);

				// Check if the source folder exists
				if (!hp.fs.existsSync(hp.path.resolve(project.path, srcPath.replace('*.ttf', '')))) {
					reject(
						new Error(
							hp.colors.red(
								`The folder ${hp.colors.cyan(
									srcPath.replace('*.ttf', '')
								)} wasn't found. Make sure the the provided name is correct and the source files are placed in the ${hp.colors.cyan(
									'ttf'
								)} subfolder.`
							)
						)
					);
				}
			}

			try {
				await generateWoffFonts(srcPath, woffDist);
				await generateWoff2Fonts(srcPath, woff2Dist);
				console.log(
					hp.colors.green(
						`\n✔ Font files successfully created at ${hp.colors.cyan(woffDist)} and ${hp.colors.cyan(
							woff2Dist
						)}\n`
					)
				);
				done();
			} catch (err) {
				reject(err);
			}
		});

		/**
		 * Generates woff version of ttf font files
		 * @param {string} entryFiles Blog for entry files
		 * @param {string} distFolder Relative of for destination folder
		 */
		function generateWoffFonts(entryFiles, distFolder) {
			return new Promise((resolve, reject) => {
				hp.pump([gulp.src(entryFiles), ttf2woff(), gulp.dest(distFolder).on('error', reject)], resolve);
			});
		}

		/**
		 * Generates woff2 version of ttf font files
		 * @param {string} entryFiles Blog for entry files
		 * @param {string} distFolder Relative of for destination folder
		 */
		function generateWoff2Fonts(entryFiles, distFolder) {
			return new Promise((resolve, reject) => {
				hp.pump([gulp.src(entryFiles), ttf2woff2(), gulp.dest(distFolder).on('error', reject)], resolve);
			});
		}
	};
};
