/**
 * A Gulp task analyzing PHP files and create a .pot file for translations
 * @param {*} gulp Instance of gulp
 * @param {object} config Tasks configurations
 * @param {object} project Project info
 * @param {object} hp Additional helper functions already imported on the gulpfile
 * @param {object} arg Object with arguments passed via command line
 */
module.exports = function (gulp, config, project, hp, arg) {
	return function () {
		return new Promise((done, reject) => {
			// Import modules
			const wpPot = require('gulp-wp-pot');

			// Check if domain override was provided
			if (arg.domain || arg.d) {
				project.id = arg.domain || arg.d;
			}
			// Check if package override was provided
			if (arg.package || arg.pkg || arg.p) {
				project.package = arg.package || arg.pkg || arg.p;
			}
			// Check if should enable the path comments
			const disableComments = !(arg['include-paths'] || false);

			hp.pump(
				[
					// Get source PHP files
					gulp.src(config.files),

					// Gather translations from source files
					wpPot({
						domain: project.id,
						package: project.package,
						noFilePaths: disableComments,
						ignoreTemplateNameHeader: true,
					}).on('error', reject),

					// Write files to destination folder
					gulp.dest(config.dist + project.id + '.pot').on('end', function () {
						console.log(
							hp.colors.green(
								`\n✔ POT file successfully generated at ${hp.colors.cyan(
									config.dist + project.id + '.pot'
								)}\n`
							)
						);
						if (!disableComments) {
							console.log(
								hp.colors.yellow(
									'! You enabled the file path comments. Please use it only for reference, DO NOT commit this file with these comments.'
								)
							);
						}
					}),
				],
				done
			);
		});
	};
};
