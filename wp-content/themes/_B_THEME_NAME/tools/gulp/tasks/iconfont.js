/**
 * A Gulp task for creating icon font from SVG files
 * @param {*} gulp Instance of gulp
 * @param {object} config Tasks configurations
 * @param {object} project Project info
 * @param {object} hp Additional helper functions already imported on the gulpfile
 * @param {object} arg Object with arguments passed via command line
 */
module.exports = function (gulp, config, project, hp, arg) {
	return function () {
		// Import modules
		const iconfont = require('gulp-iconfont');
		const replace = require('gulp-replace');
		const replaceInFile = require('replace-in-file');
		const del = require('del');

		// List of processed icons
		let icons = [];

		// Version of icons font
		let version = 0;

		return new Promise(async (done, reject) => {
			await generateIconFont();
			await updateVariables();
			await updateOtherOccurrencies();

			console.log(hp.colors.green(`\n✔ Font files successfully updated to version ${version}.`));
			console.log(hp.colors.gray('• fonts:'), hp.colors.cyan(`\t${config.dist}`));
			console.log(hp.colors.gray('• scss:'), hp.colors.cyan(`\t${config.scss}`));
			console.log(hp.colors.gray('• occurencies:'), hp.colors.cyan(`\t${config.occurrences.join(', ')}`));

			console.log(
				hp.colors.yellow(`\n! Run the ${hp.colors.bold('gulp sass')} command to update it on the dist CSS.\n`)
			);
			done();
		});

		function generateIconFont() {
			return new Promise((resolve, reject) => {
				hp.pump(
					[
						// Get source files
						gulp.src(config.entry),

						// Create font files from the SVGs
						iconfont({
							fontName: config.name,
							prependUnicode: true,
							formats: ['ttf', 'woff', 'woff2', 'svg'],
							fontHeight: 1001,
							normalize: true,
						}).on('glyphs', (glyphs) => {
							icons = glyphs;
						}),

						// Write files to destination folder
						gulp.dest(config.dist).on('error', () => {
							reject();
							return;
						}),
					],
					resolve
				);
			});
		}

		/**
		 * Update variables in the SCSS file
		 */
		function updateVariables() {
			return new Promise((resolve, reject) => {
				// Get current font version
				const scssContent = hp.fs.readFileSync(hp.path.resolve(project.path, config.scss), {
					encoding: 'utf8',
				});
				const currentVersion = parseInt(scssContent.match(/(?<=\$icons-version: )(\d+)(?=;)/)[0]);
				version = currentVersion + 1;

				// Reorder the list alphabetically
				icons.sort(function (a, b) {
					if (a.name < b.name) return -1;
					if (a.name > b.name) return 1;
					return 0;
				});

				// Builds the list of icons to be added in the SCSS array
				let iconsList = '';
				icons.forEach((icon) => {
					iconsList += `\t${icon.name}: \\${icon.unicode[0]
						.charCodeAt(0)
						.toString(16)
						.toUpperCase()
						.replace(/^E/, 'e')},\n`;
				});

				return hp.pump(
					[
						// Get source file
						gulp.src(config.scss),

						// Update icons array and font version
						replace(/\$icons:\s*\((\n|.|\t)*?\);/g, `$icons: (\n${iconsList});`),
						replace(/(?<=\$icons-version: )(\d+)(?=;)/, version),

						// Write files to destination folder
						gulp.dest(config.scss.replace(/\/[^\/]+$/, '')).on('end', () => {}),
					],
					resolve
				);
			});
		}

		/**
		 * Update all occurrances of the version on the theme code
		 */
		function updateOtherOccurrencies() {
			return new Promise(async (resolve, reject) => {
				const replaceOptions = {
					files: config.occurrences,
					from: new RegExp(`${config.name}\\.(\\w+)\\?v(\\d+)`, 'g'),
					to: `${config.name}.$1?v${version}`,
				};

				try {
					const results = await replaceInFile(replaceOptions);
					resolve();
				} catch (err) {
					reject(err);
				}
			});
		}
	};
};
