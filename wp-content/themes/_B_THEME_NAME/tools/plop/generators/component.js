const slugify = require('slugify');
const titleCase = require('titlecase');

const createComponent = (plop) => {
	plop.setGenerator('component', {
		description: 'Generate boilerplate files for a new component template',
		prompts: [
			{
				type: 'input',
				name: 'name',
				message: `What is the component name?`,
				suffix: ' (separate words by dash or space)'.gray,
				validate: (input) => {
					const pattern = new RegExp(/^[a-zA-ZÀ-ÿ][a-zA-ZÀ-ÿ0-9-\s]+$/);
					const message = `${
						'Invalid:'.red.bold
					} Should contain only letters, digits and dashes, and must start with a letter`;
					return pattern.test(input) ? true : message;
				},
				filter: (input) => {
					return slugify(input, {
						lower: true,
						strict: true,
					});
				},
			},
			{
				type: 'input',
				name: 'description',
				message: `Write a short description:`,
				suffix: ' (i.e. Header for the About page)'.gray,
				filter: (input, answers) => {
					if (!input) {
						return `Template for the ${titleCase(answers.name)} component.`;
					}
					return input.replace(/([^\.])[:;,]?$/, '$1.');
				},
			},
			{
				type: 'list',
				name: 'htmlTag',
				message: `Tag for the root element:`,
				suffix: ' (You can change it later if needed)'.gray,
				choices: [
					{ name: 'div', short: '<div>' },
					{ name: 'section', short: '<section>' },
					{ name: 'form', short: '<form>' },
					{ name: 'nav', short: '<nav>' },
					{ name: 'header', short: '<header>' },
					{ name: 'footer', short: '<footer>' },
					{ name: 'button', short: '<button>' },
				],
				default: 'div',
				loop: false,
			},
			{
				type: 'confirm',
				name: 'hasScript',
				message: `Does it need a script file?`,
				default: false,
			},
		],
		actions: (answers) => {
			const actions = [];

			// Class
			actions.push({
				type: 'add',
				templateFile: '../templates/component/class.hbs',
				path: './classes/Components/{{ pascalCase name }}.php',
			});

			// Template
			actions.push({
				type: 'add',
				templateFile: '../templates/component/template.hbs',
				path: './components/{{ lowerCase name }}/{{ name }}.php',
			});

			// Styles
			actions.push({
				type: 'add',
				templateFile: '../templates/component/styles.hbs',
				path: './components/{{ lowerCase name }}/{{ name }}.scss',
			});
			actions.push({
				type: 'add',
				template: '',
				path: `./assets/css/dist/components/{{ kebabCase name }}.dist.css`,
			});

			// Scripts
			if (answers.hasScript) {
				actions.push({
					type: 'add',
					templateFile: '../templates/component/scripts.hbs',
					path: './components/{{ lowerCase name }}/{{ name }}.js',
				});
			}

			return actions;
		},
	});
};

module.exports = createComponent;
