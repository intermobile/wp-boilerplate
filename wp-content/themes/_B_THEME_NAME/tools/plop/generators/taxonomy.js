const slugify = require('slugify');
const colors = require('colors');

const createPostType = (plop) => {
	plop.setGenerator('taxonomy', {
		description: 'Generate a class for registering a new taxonomy',
		prompts: [
			{
				type: 'input',
				name: 'singular',
				message: `What is the taxonomy name?`,
				suffix: colors.gray(
					` (in singular, i.e. ${'product category'.magenta}, ${'event type'.magenta}, etc.)`
				),
				validate: (input) => {
					const pattern = new RegExp(/^[a-zA-ZÀ-ÿ][a-zA-ZÀ-ÿ0-9-\s]*$/);
					if (!pattern.test(input)) {
						return `Should contain only letters, digits and dashes, and must start with a letter`.red;
					}
					if (input.length > 32) {
						return `The taxonomy name must not exceed 32 characters`.red;
					}

					const reserved = [
						'attachment',
						'attachment_id',
						'author',
						'author_name',
						'calendar',
						'cat',
						'category__and',
						'category__in',
						'category__not_in',
						'category_name',
						'comments_per_page',
						'cpage',
						'day',
						'embed',
						'error',
						'exact',
						'favicon',
						'feed',
						'fields',
						'hour',
						'm',
						'minute',
						'monthnum',
						'more',
						'name',
						'nopaging',
						'offset',
						'order',
						'orderby',
						'p',
						'page',
						'page_id',
						'paged',
						'pagename',
						'pb',
						'perm',
						'post__in',
						'post__not_in',
						'post_mime_type',
						'post_parent',
						'post_parent__in',
						'post_parent__not_in',
						'post_status',
						'post_type',
						'post_type',
						'posts',
						'posts_per_archive_page',
						'posts_per_page',
						'preview',
						'robots',
						's',
						'search',
						'second',
						'sentence',
						'showposts',
						'subpost',
						'subpost_id',
						'tag',
						'tag__and',
						'tag__in',
						'tag__not_in',
						'tag_id',
						'tag_slug__and',
						'tag_slug__in',
						'taxonomy',
						'tb',
						'term',
						'title',
						'w',
						'withcomments',
						'withoutcomments',
						'year',
					];
					const snakeCase = input.replace(/[\s-]/g, '_').toLowerCase();
					if (reserved.includes(snakeCase)) {
						return `${snakeCase.bold} is reserved by WordPress, please choose another name`.red;
					}

					return true;
				},
				filter: (input) => {
					return slugify(input.replace(/_/g, '-'), {
						lower: true,
						strict: true,
						replacement: '-',
					});
				},
			},
			{
				type: 'input',
				name: 'plural',
				message: `What is the name in plural?`,
				suffix: colors.gray(` (i.e. ${'product categories'.magenta}, ${'event types'.magenta}, etc.)`),
				validate: (input) => {
					const pattern = new RegExp(/^[a-zA-ZÀ-ÿ][a-zA-ZÀ-ÿ0-9-\s]*$/);
					const message = `Should contain only letters, digits and dashes, and must start with a letter`.red;
					return pattern.test(input) ? true : message;
				},
				filter: (input) => {
					return slugify(input.replace(/_/g, '-'), {
						lower: true,
						strict: true,
						replacement: '-',
					});
				},
			},
			{
				type: 'confirm',
				name: 'isHierarchical',
				message: `Is it hierarchical?`,
				suffix: ` (Whether it can have a parent or children)`.gray,
				default: true,
			},
			{
				type: 'confirm',
				name: 'showInRest',
				message: `Enable endpoint in the REST API?`,
				default: false,
			},
			{
				type: 'checkbox',
				name: 'postTypes',
				message: `For which post types should this taxonomy be available?`,
				choices: ['post', 'page', 'attachment', 'nav_menu_item'],
				loop: false,
				filter: (selected) => {
					return selected.length ? ` \'${selected.join("', '")}\' ` : '';
				},
			},
		],
		actions: (data) => {
			const actions = [];

			// Post type class
			actions.push({
				type: 'add',
				templateFile: '../templates/taxonomy/class.hbs',
				path: './classes/Taxonomies/{{ pascalCase singular }}.php',
			});

			return actions;
		},
	});
};

module.exports = createPostType;
