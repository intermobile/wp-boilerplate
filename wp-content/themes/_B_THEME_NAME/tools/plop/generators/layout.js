const slugify = require('slugify');
const colors = require('colors');
const titleCase = require('titlecase');

const createPage = (plop) => {
	plop.setGenerator('layout', {
		description: 'Generate boilerplate files for a new layout (page)',
		prompts: [
			{
				type: 'input',
				name: 'name',
				message: `What is the layout name?`,
				suffix: ' (separate words by dash or space)'.gray,
				validate: (input) => {
					const pattern = new RegExp(/^[a-zA-ZÀ-ÿ][a-zA-ZÀ-ÿ0-9-\s]+$/);
					const message = `${
						'Invalid:'.red.bold
					} Should contain only letters, digits and dashes, and must start with a letter`;
					return pattern.test(input) ? true : message;
				},
				filter: (input) => {
					return slugify(input, {
						lower: true,
						strict: true,
					});
				},
			},
			{
				type: 'list',
				name: 'type',
				message: `What is the type of the page?`,
				choices: [
					{ name: 'Page Template', value: 'page' },
					{ name: 'Custom Post Type' + colors.gray(' (Single)'), value: 'post' },
					{ name: 'Custom Taxonomy', value: 'taxonomy' },
					{ name: 'Tag' + colors.gray(' (Default)'), value: 'tag' },
					{ name: 'Author', value: 'author' },
					{ name: 'Blog' + colors.gray(' (Index)'), value: 'blog' },
				],
			},
			{
				type: 'input',
				name: 'displayName',
				message: `What's the name to display in the templates list?`,
				suffix: ' (Leave it empty to use the same)'.gray,
				filter: (input, answers) => {
					return input !== '' ? input : titleCase(answers.name.replace(/-/g, ' '));
				},
				validate: (input) => {
					return input.charAt(0) === input.charAt(0).toUpperCase()
						? true
						: `The first letter must be in uppercase (i.e. 'Ecommerce Cart', 'Campaign Landing', etc.). \n   In this case, the way you write is exactly how it will appear in the templates list.`
								.yellow;
				},
				when: (answers) => {
					return answers.type === 'page';
				},
			},
			{
				type: 'input',
				name: 'postType',
				message: `What is the Post Type name?`,
				suffix: colors.gray(
					` (exactly as registered, i.e. ${'product'.magenta}, ${'recipe'.magenta}, ${'faq'.magenta}, etc.)`
				),
				validate: (input) => {
					const pattern = new RegExp(/^[a-z_][a-z0-9-_]*$/);
					if (!pattern.test(input)) {
						return `Should contain only letters, digits and dashes, and must start with a letter`.red;
					}
					return true;
				},
				when: (answers) => {
					return answers.type === 'post';
				},
			},
			{
				type: 'input',
				name: 'taxonomy',
				message: `What is the Taxonomy name?`,
				suffix: colors.gray(` (exactly as registered)`),
				validate: (input) => {
					const pattern = new RegExp(/^[a-z_][a-z0-9-_]*$/);
					if (!pattern.test(input)) {
						return `Should contain only letters, digits and dashes, and must start with a letter`.red;
					}
					return true;
				},
				when: (answers) => {
					return answers.type === 'taxonomy';
				},
			},
			/* {
				type: 'checkbox',
				name: 'includes',
				message: `Does it include any of these components?`,
				choices: [
					{
						name: `Breadcrumbs ${'– Includes the breadcrumbs component in the page and its styles.'.gray}`,
						short: 'Breadcrumbs',
						value: 'breadcrumbs',
					},
					{
						name: `Swiper ${'– Includes most common styles for Swiper carousels and sliders.'.gray}`,
						short: 'Swiper',
						value: 'swiper',
					},
				],
			}, */
		],
		actions: (data) => {
			const actions = [];

			// Configuration for the entry PHP file from each type
			const entries = {
				page: {
					filename: `template-{{ kebabCase name }}`,
					title: `Template name: {{ displayName }}`,
				},
				post: {
					filename: `single-{{ postType }}`,
					title: `{{ titleCase name }} post type template`,
				},
				taxonomy: {
					filename: `taxonomy-{{ taxonomy }}`,
					title: `{{ titleCase name }} taxonomy template`,
				},
				tag: {
					filename: `tag`,
					title: `Tag template`,
				},
				author: {
					filename: `author`,
					title: `Author template`,
				},
				blog: {
					filename: `home`,
					title: `Blog listing`,
				},
			};

			// Adds boolean props that define the inclues
			/* const includes = {
				includesBreadcrumbs: data.includes.includes('breadcrumbs'),
				includesSwiper: data.includes.includes('swiper'),
			}; */

			// PHP Class
			actions.push({
				type: 'add',
				templateFile: '../templates/layout/class.hbs',
				path: './classes/Layouts/{{ pascalCase name }}.php',
			});

			// Template
			actions.push({
				type: 'add',
				templateFile: '../templates/layout/template.hbs',
				path: `./layouts/{{ kebabCase name }}/{{ kebabCase name }}.php`,
				/* data: includes, */
			});

			// Styles
			actions.push({
				type: 'add',
				templateFile: '../templates/layout/styles.hbs',
				path: `./layouts/{{ kebabCase name }}/{{ kebabCase name }}.scss`,
				/* data: includes, */
			});
			actions.push({
				type: 'add',
				template: '',
				path: `./assets/css/dist/layouts/{{ kebabCase name }}.dist.css`,
			});

			// Entry JS
			actions.push({
				type: 'add',
				templateFile: '../templates/layout/scripts.hbs',
				path: `./layouts/{{ kebabCase name }}/{{ kebabCase name }}.js`,
				/* data: includes, */
			});

			// Entry PHP File
			actions.push({
				type: 'add',
				templateFile: '../templates/layout/entry-php.hbs',
				path: `./${entries[data.type].filename}.php`,
				data: {
					isLoopItem: ['page', 'post'].includes(data.type),
					entryTitle: plop.renderString(entries[data.type].title, data),
				},
			});

			return actions;
		},
	});
};
module.exports = createPage;
