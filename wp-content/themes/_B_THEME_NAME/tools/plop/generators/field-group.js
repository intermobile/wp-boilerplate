const slugify = require('slugify');
const colors = require('colors');

const createFieldGroup = (plop) => {
	plop.setGenerator('field-group', {
		description: 'Generate a new FieldGroup class for ACFs registering',
		prompts: [
			{
				type: 'input',
				name: 'name',
				message: `What is the field group name?`,
				suffix: ' (separate words by dash or space)'.gray,
				validate: (input) => {
					const pattern = new RegExp(/^[a-zA-ZÀ-ÿ][a-zA-ZÀ-ÿ0-9-\s]*$/);
					const message = `Should contain only letters, digits and dashes, and must start with a letter`.red;
					return pattern.test(input) ? true : message;
				},
				filter: (input) => {
					return slugify(input, {
						lower: true,
						strict: true,
					});
				},
			},
		],
		actions: (data) => {
			const actions = [];

			// Field Group class
			actions.push({
				type: 'add',
				templateFile: '../templates/field-group/class.hbs',
				path: './classes/FieldGroups/{{ pascalCase name }}FieldGroup.php',
			});

			return actions;
		},
	});
};

module.exports = createFieldGroup;
