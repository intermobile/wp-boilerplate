<?php
/**
 * Template name: Style Guide
 *
 * Entry file for the Style Guide page
 *
 * @package Theme\Layouts
 */

use Theme\Layouts\StyleGuide;

the_post();

get_header();

echo new StyleGuide();

get_footer();