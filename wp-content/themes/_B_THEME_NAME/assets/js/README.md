# Javascript Guide

> Make sure you read the [General README.md](../../../../../README.md) file that contains some information about the javascript structure.

This file includes some basic information about developing **Javascript** components in this theme.

## Summary

-   [Coding](#user-content-coding)
-   [Globals](#user-content-globals)
-   [Components](#user-content-components)
-   [Pages](#user-content-pages)
-   [Libraries](#user-content-libraries)

## Entry files

We use _webpack_ to bundle and minify all scripts into one compiled file for each page template. The entry files to these bundles are the JS files from each page folder at `./layouts/[layout-name]`. These import all needed components and trigger their respective context functions.

## Coding

The scripts are mainly written using [ES6 features](http://es6-features.org/#Constants) and [jQuery](https://api.jquery.com/).

Scripts for components inherits on the `Component` class (from `./assets/js/utils/component.js`), while scripts for pages inherits from the `Page` class (from `./assets/js/utils/page.js`).

### Page

In the page JS, we basically:

1. Import of the `Page` class;
2. Imports from all needed components in this page;
3. Create a new `Page` instance, passing the imported components in an array as argument.

Here's an example of a page JS:

```js
import { Page } from '@/utils/page';

// Import all needed components
import Essentials from '@/core/essentials';
import SectionHeading from '@/components/section-heading/section-heading';

// Initialize all components
new Page([SectionHeading]);
```

> Take a look at the `Page` [class script](utils/page.js) to better understand what it actually does.

### Component

For components, we do:

1. Import the `Component` class to inherit from;
2. Import **libraries** and **helpers** that will be needed in the file (like _jQuery_, _Swiper_, _Axios_, etc);
3. Define the component class, setting main DOM references and general variables in its constructor;
4. Add the needed scripts in the `pageLoaded()` function (that runs after the DOM load event), or in other available functions from the `Component` class, like `windowResize()` and `pageScrolled()`;
5. Register other internal functions when needed (for better code organisation);
6. Export an instance of the component that will be imported in the page JS;

Here's an example of a component JS:

```js
import $ from 'jquery';
import { Component } from '@/utils/component';

class PostsCarousel extends Component {
	constructor() {
		super();

		// DOM refs and logic variables
		this.selfRef = $('._posts-carousel');
	}

	pageLoaded() {
		if (this.selfRef.length) {
			// Code comes here
		}
	}
}

export default new PostsCarousel();
```

As the file exports an instance of the component, you can access methods of this component class in other components as well, or use custom events to communication between components.

## Components

Each component has a JS file on its respective folder, at `./components/[component-name]/`. They are used to add interactivity each the component that needs it, like mobile menu toggling, parallax effect, animations and more.

The components are called in the entry file of each page to be initialized and to trigger their inherited events, like, `scriptLoaded()`, `pageLoaded()`, `pageScrolled()`, `windowResized()`.

## Globals

In the `globals.js` from the `./assets/js/utils/` folder, you can add global variables that can be used on any component, like jQuery references for the `body` and `window` objects and more.

## Libraries

Most libraries are imported as `npm` modules. To install a new module follow these steps:

1. Install the library `npm` module via:

```shell
$ npm install library-name
```

2. Add the library in the `webpack.config.js`, inside the `plugins` array (if the library should be embeded in the bundle) or in the `externals` array (if you need to reference to the library on your code, but will import it externally, like from CDN or single file). To add the library properly you should reference it using its **symbol identifier** (name of the variable used in code) and npm **module name** (name of installed module). Like: `$`: '`jquery`' or `Swiper`: '`swiper`'.

3. Import the library in the component that needs it.

If the library you need does't exist in the `npm` repository, you can import it via URL in the `./classes/Hooks/Enqueues.php` file (from theme's root).
