// Import components needed on all pages
import MobileMenu from '@/components/mobile-menu/mobile-menu';

// Export an array of essential components
export default [MobileMenu];
