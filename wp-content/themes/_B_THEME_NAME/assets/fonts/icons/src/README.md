# Icons

If you want to add or modify an icon to the `icons` font file, you will need to follow some steps:

## Modifying an icon

You can modify any icon by changing the content of its respective SVG file in the `svg` folder. Any changes to these files will reflect in the icon font once the `gulp iconfont` task is run.

## Adding a new icon

For adding new icons, you need to create a folder named `new` at the `./assets/fonts/icons/src` directory and add the new SVG files in this folder.

The SVG file name will be used as the class name for the icon, so choose names like `menu.svg`, `user.svg`, `profile.svg`, etc.

> There is no need to add the char code in the name (like `uEA0A`) as this is added automatically by the gulp task.

Once your new icons are placed in the new folder it's time to update the font files and references.

## Updating the font files

To update the icons' font, run the `gulp iconfont` task on a terminal on the theme directory.

This task will:

-   Update the icons' font files;
-   Move the icons from the `new` folder to the `svg` folder after processing them with the correct naming;
-   Update the `$icons` array and `$version` number in the `_icons.scss` file.
-   Update other occurrencies of the version, like in preload links.

## Additional info

The `gulp iconfont` task looks for all icons in the svg folders defined in the `tasks.iconfont.entry` array at the `tools/gulp/config.json` file.
