<?php
/**
 * Footer
 *
 * The template for displaying the site footer and colisng HTML tags
 * It contains the closing of the #content div and all content after.
 *
 * @package Theme
 */

use Theme\Components\ContainerGuide;
use Theme\Components\Footer;

?>
</div><!-- #content -->

<?php
echo new Footer();

// Adds a guide line to help layout adjustments (for development)
if ( isset( $_GET['guide'] ) ) :
	echo new ContainerGuide();
endif;
?>

</div><!-- #page -->

<?php wp_footer(); ?>

</body>

</html>