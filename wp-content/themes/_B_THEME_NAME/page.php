<?php
/**
 * Page template
 *
 * The default template por WP pages
 *
 * @package Theme
 */

use Theme\Layouts\Page;

get_header();

the_post();

echo new Page();

get_footer();
