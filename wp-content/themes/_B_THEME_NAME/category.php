<?php
/**
 * Category template
 *
 * Template for displaying a posts category page
 *
 * @package Theme
 */

use Theme\Layouts\Category;
use Theme\Layouts\Subcategory;

get_header();

global $cat;
$category = get_category( $cat );

if ( $category->parent > 0 ) {
	echo new Subcategory();
} else {
	echo new Category();
}

get_footer();
