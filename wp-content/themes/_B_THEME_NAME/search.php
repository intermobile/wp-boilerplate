<?php
/**
 * Search template
 *
 * The template for displaying the Search Results page
 *
 * @package Theme
 */

use Theme\Layouts\Search;

get_header();

echo new Search();

get_footer();
