<?php
/**
 * Front-page template
 *
 * The template for displaying the website homepage (WP Front-page)
 *
 * @package Theme
 */

use Theme\Layouts\Home;

get_header();

echo new Home();

get_footer();
