<?php
/**
 * Default Page
 *
 * Template for the Default page
 *
 * @package Theme\Layouts
 */

use Theme\Components\RichContent;
use Theme\Components\PageHeader;

?>
<div id="page-layout" class="layout-content">
	<main>

		<?php
		// PAGE TITLE
		echo new PageHeader(
            array(
				'title' => $page_title,
            )
		);
		?>

		<!-- PAGE CONTENT -->
		<article class="container">
			<?php
			echo new RichContent(
				array(
					'content' => $content,
				)
			);
			?>
		</article>

	</main>
</div>