<?php
/**
 * Front-page template
 *
 * The template for displaying the website homepage (WP Front-page)
 *
 * @package Theme\Layouts
 */

use Theme\Components\ArticlesList;

?>
<div id="home-layout" class="layout-content">
	<main>
		<?php
        echo new ArticlesList(
			array(
				'class'     => 'pt-6 pb-4',
				'title'     => __( 'Latest Articles', '_B_THEME_NAME' ),
				'link_text' => __( 'View all articles', '_B_THEME_NAME' ),
				'link_href' => '#',
				'articles'  => $latest_articles,
			)
		);
        ?>
	</main>
</div>
