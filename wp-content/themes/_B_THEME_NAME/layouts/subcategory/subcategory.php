<?php
/**
 * Subcategory Page
 *
 * Template for the Subcategory page
 *
 * @package Theme\Layouts
 */

use Theme\Components\ArticlesList;
use Theme\Components\PageHeader;

?>
<div id="subcategory-layout" class="layout-content">
	<main>

		<?php
		// PAGE TITLE
		echo new PageHeader(
            array(
				'title'       => $subcategory->name,
				'description' => $subcategory->description,
            )
		);

		// CATEGORY ARTICLES
		echo new ArticlesList(
            array(
				'class'    => 'pt-6 pb-4',
				'articles' => $articles,
            )
		);
		?>

	</main>
</div>
