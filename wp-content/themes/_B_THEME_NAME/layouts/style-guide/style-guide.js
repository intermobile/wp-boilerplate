import { Page } from '@/utils/page';

// Import all needed components
import Essentials from '@/core/essentials';

// Initialize all components
new Page(Essentials.concat([]));
