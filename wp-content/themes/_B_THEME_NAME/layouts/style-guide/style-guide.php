<?php
/**
 * Template for the Theme\Layouts\Style Guide page
 *
 * @package Theme\Layouts
 */

use Theme\Components\StyleSpecs;

?>
<div id="style-guide-page" class="layout-content">
	<main>
		<?php echo new StyleSpecs( array( 'class' => 'my-6' ) ); ?>
	</main>
</div>