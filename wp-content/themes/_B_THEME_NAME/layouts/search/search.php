<?php
/**
 * Search Results Page
 *
 * Template for the Search results page
 *
 * @package Theme\Layouts
 */

use Theme\Components\ArticlesList;
use Theme\Components\PageHeader;

?>
<div id="search-layout" class="layout-content">
	<main>
		<?php
		// SEARCH TITLE
		echo new PageHeader(
            array(
				// translators: %s is a placeholder for the searched term
				'title' => sprintf( __( 'Results for %s', '_B_THEME_NAME' ), "<span class=\"searched-term\">{$searched_term}</span>" ),
            )
		);

		// RESULT ARTICLES
		echo new ArticlesList(
            array(
				'class'    => 'pt-6 pb-4',
				'articles' => $articles,
            )
		);
		?>
	</main>
</div>
