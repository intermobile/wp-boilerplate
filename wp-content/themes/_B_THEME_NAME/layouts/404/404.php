<?php
/**
 * 404 Page
 *
 * Template for the 404 page (page not found)
 *
 * @package Theme\Layouts
 */

use Theme\Components\NotFoundContent;

?>
<div id="page-not-found" class="layout-content">
	<main>

		<!-- 404 CONTENT -->
		<?php echo new NotFoundContent(); ?>

	</main>
</div>
