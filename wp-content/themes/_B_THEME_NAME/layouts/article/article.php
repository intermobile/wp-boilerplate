<?php
/**
 * Article Page
 *
 * Template for the Article page
 *
 * @package Theme\Layouts
 */

use Theme\Components\ArticleHeader;
use Theme\Components\RichContent;
use Theme\Components\Sidebar;

?>
<div id="article-layout" class="layout-content">
	<main>

		<header class="container mb-6">
			<?php
			// TITLE, INFO & FEATURED IMAGE
			echo new ArticleHeader(
				array(
					'article_data' => $article_data,
				),
			);
			?>
		</header>

		<div class="container">
			<div class="row">

				<article class="col-12 col-lg-8">
					<?php
					// CONTENT
					echo new RichContent(
						array(
							'content' => $article_data->content,
						),
					);
					?>
				</article>

				<?php
				// SIDEBAR
				echo new Sidebar(
					array(
						'class' => 'col-12 col-lg-4',
					)
				);
				?>

			</div>
		</div>

	</main>
</div>
