import { Page } from '../../assets/js/utils/page';

// Import all needed components
import Essentials from '../../assets/js/core/essentials';

// Initialize all components
new Page(Essentials.concat([]));
