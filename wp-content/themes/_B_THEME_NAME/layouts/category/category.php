<?php
/**
 * Category Page
 *
 * Template for the Category page
 *
 * @package Theme\Layouts
 */

use Theme\Components\ArticlesList;
use Theme\Components\PageHeader;

?>
<div id="category-layout" class="layout-content">
	<main>

		<?php
		// PAGE TITLE
		echo new PageHeader(
			array(
				'title'       => $category->name,
				'description' => $category->description,
			)
		);

		// CATEGORY ARTICLES
        echo new ArticlesList(
			array(
				'class'    => 'pt-6 pb-4',
				'articles' => $articles,
			)
		);
		?>

	</main>
</div>
