<?php
/**
 * Header template
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @package Theme
 */

use Theme\Components\Analytics;
use Theme\Components\Favicons;
use Theme\Components\Header;
use Theme\Components\PreLinks;
use Theme\Helpers\ThemeUtils;

?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>

<head>
	<?php
	// Google Analytics
	if ( ThemeUtils::is_production() ) {
		echo new Analytics();
	}
	?>

	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">

	<?php if ( ! ThemeUtils::is_production() ) : ?>
	<meta name="robots" content="noindex" />
	<?php endif; ?>

	<!-- Color for the Android Chrome title bar -->
	<!-- <meta name="theme-color" content="#ffffff"> -->

	<link rel="profile" href="http://gmpg.org/xfn/11">

	<?php
	// Preconnects, Preloads and Prefetches
	echo new PreLinks();

	// Favicons
	echo new Favicons();

	wp_head();
	?>
</head>

<body <?php body_class(); ?> template-name="">

	<div id="page">
		<a id="skip-link" class="btn btn-primary visually-hidden-focusable"
			href="#content"><?php esc_html_e( 'Skip to content', '_B_THEME_NAME' ); ?></a>

		<!-- HEADER -->
		<?php echo new Header(); ?>

		<div id="content">