<?php
/**
 * Post template
 *
 * The template for displaying posts of type article (default WordPress posts)
 *
 * @package Theme
 */

use Theme\Layouts\Article;

get_header();

the_post();

echo new Article();

get_footer();
