<?php
/**
 * 404 page template
 *
 * The template for displaying 404 pages (not found)
 *
 * @package Theme
 */

use Theme\Layouts\NotFound;

get_header();

echo new NotFound();

get_footer();
