## Localization

Place your theme language files in this directory.

You can use the `gulp pot` task to generate an updated `_B_THEME_NAME.pot` file, and `gulp mo` to compile the `.po` files.

Please visit the following links to learn more about translating WordPress themes:

https://make.wordpress.org/polyglots/teams/
https://developer.wordpress.org/themes/functionality/localization/
https://developer.wordpress.org/reference/functions/load_theme_textdomain/
