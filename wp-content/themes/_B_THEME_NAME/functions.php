<?php
/**
 * Functions
 *
 * Setup functions and theme definitions
 *
 * @see https://developer.wordpress.org/themes/basics/theme-functions/
 * @see https://gitlab.com/intermobile/wp-solidify
 *
 * @package Theme
 */

use Solidify\Core\Theme;
use Solidify\Core\WPTemplate;
use Theme\Helpers\SiteActions;

// Composer autoload
require get_template_directory() . '/vendor/autoload.php';

// Action controller
$input_body = file_get_contents( 'php://input' );
if ( strpos( $input_body, 'site_action' ) ) {
	$request = json_decode( $input_body );
	if ( is_object( $request ) ) {
		SiteActions::controller_action( $request );
	}
}

$registrable_namespaces = array();

// Check if ACF plugin is active to register fields
if ( function_exists( 'acf_add_local_field_group' ) ) {
	$registrable_namespaces[] = 'FieldGroups';
	$registrable_namespaces[] = 'Options';
}

// Set core registrables
$registrable_namespaces = array_merge(
    $registrable_namespaces,
    array(
		'Taxonomies',
		'PostTypes',
		'Hooks',
	)
);

// Setup a theme instance for WP Solidify
global $theme_class;
$theme_class = new Theme(
    array(
		'template_engine'        => new WPTemplate(),
		'namespace'              => 'Theme',
		'base_folder'            => 'classes',
		'registrable_namespaces' => $registrable_namespaces,
    )
);
