<?php

namespace Theme\FieldGroups;

use Solidify\Core\FieldGroup;
use Solidify\Core\OptionsPage;
use Solidify\Fields;
use Theme\CustomFields\WebsiteFooterFields;

class ThemeOptionsFieldGroup extends FieldGroup { // phpcs:ignore
	public function __construct() { // phpcs:ignore
		$footer = new WebsiteFooterFields();

		$this->set_fields(
			array(
				'footer_tab' => new Fields\Tab( 'Footer' ),
				$footer->fields,
			)
		);

		$this->args = array(
			'key'      => 'theme-options',
			'title'    => 'Theme Options',
			'style'    => 'seamless',
			'location' => array(
				array(
					OptionsPage::is_equal_to( 'theme-options' ),
				),
			),
		);
	}
}