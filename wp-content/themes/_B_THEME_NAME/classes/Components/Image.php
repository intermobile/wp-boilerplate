<?php

namespace Theme\Components;

use Theme\Helpers\Component;
use Theme\Helpers\Enqueues;

/**
 * Image element with some handlers like lazy loading and placholder
 *
 * @package Theme\Components
 */
class Image extends Component {
	public function __construct( $args = array() ) { // phpcs:ignore
		$this->name     = 'image';
		$this->template = "components/{$this->name}/{$this->name}";

		Enqueues::enqueue_component_styles( $this->name );

		// List of props passed to the template as variables
		$this->props = array_merge(
			array(
				'class'  => '', // Additional CSS class for the root element
				'url'    => '', // URL for the image
				'width'  => 0, // Image width
				'height' => 0, // Image height
				'alt'    => '', // Alternative text for the image
				'attr'   => array(), // Any other HTML attribute to be added in the <img> tag
			),
			$args
		);
	}

	/**
	 * Returns the attributes as an HTML string
     *
	 * @param array $attr_array Array with key and value for each HTML attribute.
	 * @return string
	 */
	public static function get_attributes_string( $attr_array = array() ) {
		$attr_html = '';
		foreach ( $attr_array as $attr => $value ) {
			$attr_html .= " {$attr}=\"{$value}\"";
		};
		return $attr_html;
	}
}
