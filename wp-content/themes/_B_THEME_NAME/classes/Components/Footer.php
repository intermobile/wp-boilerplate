<?php

namespace Theme\Components;

use Theme\Helpers\Component;
use Theme\Helpers\Enqueues;

/**
 * The website footer section
 *
 * @package Theme\Components
 */
class Footer extends Component {
	public function __construct( $args = array() ) { // phpcs:ignore
		$this->name     = 'footer';
		$this->template = "components/{$this->name}/{$this->name}";

		Enqueues::enqueue_component_styles( $this->name );

		// List of props passed to the template as variables
		$this->props = array_merge(
			array(
				'class'          => '', // Additional CSS class for the root element
				'copyright_text' => str_replace( '[%YEAR%]', gmdate( 'Y' ), get_field( 'footer-copyright_text', 'option' ) ),
			),
			$args
		);
	}
}
