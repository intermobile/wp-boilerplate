<?php

namespace Theme\Components;

use Theme\Helpers\Component;

/**
 * Site favicons displayed on header template file.
 *
 * @package Theme\Components
 */
class Favicons extends Component {
	public function __construct( $args = array() ) { // phpcs:ignore
		$this->name     = 'favicons';
		$this->template = "components/{$this->name}";

		// List of props passed to the template as variables
		$this->props = array_merge(
			array(
				'path'      => get_template_directory_uri() . '/assets/img/favicons', // Path of the favicon images
				'site_name' => get_bloginfo( 'name' ), // Name of the site
			),
			$args
		);
	}
}
