<?php

namespace Theme\Components;

use Theme\Helpers\Component;

/**
 * Define Preconnects, Preloads and Prefetches
 *
 * @see https://developers.google.com/web/fundamentals/performance/resource-prioritization
 *
 * @package Theme\Components
 */
class PreLinks extends Component {
	public function __construct( $args = array() ) { // phpcs:ignore
		$this->name     = 'pre-links';
		$this->template = "components/{$this->name}";

		// List of props passed to the template as variables
		$this->props = array_merge(
			array(
				'fonts_path' => get_template_directory_uri() . '/assets/fonts', // (string) URI for the fonts folder
			),
			$args
		);
	}
}
