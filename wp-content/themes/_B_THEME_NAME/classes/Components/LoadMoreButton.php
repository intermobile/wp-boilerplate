<?php

namespace Theme\Components;

use Theme\Helpers\Component;
use Theme\Helpers\Enqueues;

/**
 * Load More Button
 *
 * A button with logic to load more posts from a given context
 */
class LoadMoreButton extends Component
{ // phpcs:ignore
	public function __construct( $args = array() ) 	{ // phpcs:ignore
		$this->name     = 'load-more-button';
		$this->template = "components/{$this->name}/{$this->name}";

		Enqueues::enqueue_component_styles( $this->name );

		// List of props passed as variables to the template
		$this->props = array_merge(
			array(
				'button_text' => '' !== $this->props['button_text'] ? $this->props['button_text'] : __( 'Load More', '' ),
				'class'       => '', // Additional CSS class for the root element
			),
			$args
		);
	}
}
