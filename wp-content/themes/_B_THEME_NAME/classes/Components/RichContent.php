<?php

namespace Theme\Components;

use Theme\Helpers\Component;
use Theme\Helpers\Enqueues;

/**
 * A component for wrapping content from a WYSIWYG editor
 *
 * @package Theme\Components
 */
class RichContent extends Component {
	public function __construct( $args = array() ) { // phpcs:ignore
		$this->name     = 'rich-content';
		$this->template = "components/{$this->name}/{$this->name}";

		Enqueues::enqueue_component_styles( $this->name );

		// Apply WP filters and more if needed
		$args['content'] = $this->apply_content_filters( $args['content'] );

		// List of props passed to the template as variables
		$this->props = array_merge(
			array(
				'class'   => '', // Additional CSS class for the root element
				'content' => '', // HTML content to be rendered
			),
			$args
		);
	}

	/**
	 * Receive the page content and run some WP actions and filtering
	 *
	 * @param string $content The content of the page from the CMS, usually get_the_content().
	 */
	private static function apply_content_filters( $content ) {

		// Apply some native WP adaptation to the content
		$the_content = apply_filters( 'the_content', $content );

		// Replace Gutenberg embed classes by Bootstrap's equivalent
		$the_content = str_replace(
			array( 'wp-has-aspect-ratio', 'wp-embed-aspect-16-9' ),
			array( 'ratio', 'ratio-16x9' ),
			$the_content
		);

		return $the_content;
	}
}
