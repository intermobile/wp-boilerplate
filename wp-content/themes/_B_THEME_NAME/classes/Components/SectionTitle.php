<?php

namespace Theme\Components;

use Theme\Helpers\Component;
use Theme\Helpers\Enqueues;

/**
 * Title for a section of a page
 *
 * @package Theme\Components
 */
class SectionTitle extends Component {
	public function __construct( $args = array() ) { // phpcs:ignore
		$this->name     = 'section-title';
		$this->template = "components/{$this->name}/{$this->name}";

		Enqueues::enqueue_component_styles( $this->name );

		// List of props passed to the template as variables
		$this->props = array_merge(
			array(
				'class'     => '', // Additional CSS class for the root element
				'title'     => '', // Display title of the section.
				'link_text' => '', // Text of the link
				'link_href' => '', // Target URL of the link
			),
			$args
		);
	}
}
