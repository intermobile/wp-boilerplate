<?php

namespace Theme\Components;

use Theme\Helpers\Component;
use Theme\Helpers\Enqueues;

/**
 * Main information about the article, placed at the top of the page
 *
 * @package Theme\Components
 */
class ArticleHeader extends Component {
	public function __construct( $args = array() ) { // phpcs:ignore
		$this->name     = 'article-header';
		$this->template = "components/{$this->name}/{$this->name}";

		Enqueues::enqueue_component_styles( $this->name );

		// List of props passed to the template as variables
		$this->props = array_merge(
			array(
				'class'        => '', // Additional CSS class for the root element
				'article_data' => array(), // Article data as ArticleObject.
			),
			$args
		);
	}
}
