<?php

namespace Theme\Components;

use Theme\Helpers\Component;
use Theme\Helpers\Enqueues;

/**
 * A block with basic information about an article, linking to the article page.
 *
 * @package Theme\Components
 */
class ArticleCard extends Component {
	public function __construct( $args = array() ) { // phpcs:ignore
		$this->name     = 'article-card';
		$this->template = "components/{$this->name}/{$this->name}";

		Enqueues::enqueue_component_styles( $this->name );

		// List of props passed to the template as variables
		$this->props = array_merge(
			array(
				'class'        => '', // Additional CSS class for the root element
				'article_data' => array(), // Article data using the ArticleObject object
			),
			$args
		);
	}
}