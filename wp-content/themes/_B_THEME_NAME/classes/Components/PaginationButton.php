<?php

namespace Theme\Components;

use Theme\Helpers\Component;
use Theme\Helpers\Enqueues;

class PaginationButton extends Component { // phpcs:ignore
	public function __construct($args = array()) { // phpcs:ignore
		$this->name     = 'pagination-button';
		$this->template = "components/{$this->name}/{$this->name}";

		Enqueues::enqueue_component_styles( $this->name );

		add_action( 'Pagination', 'handle_pagination_links', 10, 2 );

		// List of props passed as variables to the template
		$this->props = array_merge(
			array(
				'class' => '', // Additional CSS class for the root element
			),
			$args
		);
	}
}
