<?php

namespace Theme\Components;

use Theme\Helpers\Component;
use Theme\Helpers\Enqueues;

/**
 * Top header bar, including logo and site navigation
 *
 * @package Theme\Components
 */
class Header extends Component {
	public function __construct( $args = array() ) { // phpcs:ignore
		$this->name     = 'header';
		$this->template = "components/{$this->name}/{$this->name}";

		Enqueues::enqueue_component_styles( $this->name );

		// List of props passed to the template as variables
		$this->props = array_merge(
			array(
				'class'     => '', // Additional CSS class for the root element
				'logo_tag'  => is_front_page() ? 'h1' : 'div', // Tag for the logo element (i.e. h1 or div)
				'site_name' => get_bloginfo( 'name' ), // Title of the site to use in the logo
			),
			$args
		);
	}
}
