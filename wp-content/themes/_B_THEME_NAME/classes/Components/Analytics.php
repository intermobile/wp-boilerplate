<?php

namespace Theme\Components;

use Theme\Helpers\Component;
use Theme\Helpers\Enqueues;

/**
 * Scripts for site analytics printed on the template header file
 *
 * @see https://developers.google.com/analytics/devguides/collection/gtagjs
 *
 * @package Theme\Components
 */
class Analytics extends Component {
	public function __construct( $args = array() ) { // phpcs:ignore
		$this->name     = 'analytics';
		$this->template = "components/{$this->name}/{$this->name}";

		Enqueues::enqueue_component_styles( $this->name );

		// List of props passed to the template as variables
		$this->props = array_merge(
			array(
				'ua_code' => '', // Property ID for Google Tag Manager
			),
			$args
		);
	}
}
