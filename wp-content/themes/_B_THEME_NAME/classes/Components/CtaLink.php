<?php

namespace Theme\Components;

use Theme\Helpers\Component;
use Theme\Helpers\Enqueues;

/**
 * A common component for links using the ACF Link field data.
 *
 * @package Theme\Components
 */
class CtaLink extends Component { // phpcs:ignore
	public function __construct($args = array()) { // phpcs:ignore
		$this->name     = 'cta-link';
		$this->template = "components/{$this->name}/{$this->name}";

		Enqueues::enqueue_component_styles( $this->name );

		// Converts the array into an object
		$args['link'] = $args['link'] ? (object) $args['link'] : null;

		// Set the default rel attribute if not defined
		if ( $args['link'] && ! property_exists( $args['link'], 'target' ) ) {
			$args['link']->target = '';
		}

		// Set the default rel attribute if not defined
		if ( $args['link'] && ! property_exists( $args['link'], 'rel' ) ) {
			$args['link']->rel = $args['link']->target === '_blank' ? 'nofollow noreferrer' : '';
		}

		// List of props passed as variables to the template
		$this->props = array_merge(
			array(
				'class'     => '', // CSS class for the component root element.
				'btn_class' => 'btn btn-primary', // CSS class for the button style. If passed, this will be overrided.
				'link'      => null, // Should receive array data from ACF Link field, with 'title', 'url', 'target' and 'rel' (additionally).
			),
			$args
		);
	}
}
