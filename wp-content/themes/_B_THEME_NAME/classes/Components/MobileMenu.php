<?php

namespace Theme\Components;

use Theme\Helpers\Component;
use Theme\Helpers\Enqueues;

/**
 * A toggleable site navigation menu displayed on smaller screen devices
 *
 * @package Theme\Components
 */
class MobileMenu extends Component {
	public function __construct( $args = array() ) { // phpcs:ignore
		$this->name     = 'mobile-menu';
		$this->template = "components/{$this->name}/{$this->name}";

		Enqueues::enqueue_component_styles( $this->name );

		// List of props passed to the template as variables
		$this->props = array_merge(
			array(
				'class' => '', // Additional CSS class for the root element
			),
			$args
		);
	}
}
