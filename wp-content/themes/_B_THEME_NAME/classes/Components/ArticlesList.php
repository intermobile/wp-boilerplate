<?php

namespace Theme\Components;

use Theme\Helpers\Component;
use Theme\Helpers\Enqueues;

/**
 * Lists a limited number or post cards
 *
 * @package Theme\Components
 */
class ArticlesList extends Component {
	public function __construct( $args = array() ) { // phpcs:ignore
		$this->name     = 'articles-list';
		$this->template = "components/{$this->name}/{$this->name}";

		Enqueues::enqueue_component_styles( $this->name );

		// List of props passed to the template as variables
		$this->props = array_merge(
			array(
				'class'     => '', // Additional CSS class for the root element
				'title'     => '', // Title of the section
				'link_text' => '', // Text for an additional link in the SectionTitle component
				'link_href' => '', // URL for an additional link in the SectionTitle component
				'articles'  => array(), // Data from each article as ArticleObject
			),
			$args
		);
	}
}
