<?php

namespace Theme\Components;

use Theme\Helpers\Component;
use Theme\Helpers\Enqueues;

/**
 * Content displayed in the 404 page
 *
 * @package Theme\Components
 */
class NotFoundContent extends Component {
	public function __construct( $args = array() ) { // phpcs:ignore
		$this->name     = 'not-found-content';
		$this->template = "components/{$this->name}/{$this->name}";

		Enqueues::enqueue_component_styles( $this->name );

		// List of props passed to the template as variables
		$this->props = array_merge(
			array(
				'class'    => '', // Additional CSS class for the root element
				'title'    => __( 'Page not found', '_B_THEME_NAME' ), // Title text
				'intro'    => __( "We couldn't find the page you're looking for.", '_B_THEME_NAME' ), // Intro text
				'cta_link' => array(
					'title'  => __( 'Go to homepage', '_B_THEME_NAME' ),
					'url'    => get_home_url( '/' ),
					'target' => '',
				),
			),
			$args
		);
	}
}
