<?php

namespace Theme\Components;

use Theme\Helpers\Component;
use Theme\Helpers\Enqueues;

/**
 * A social media list that uses a the native wp_nav_menu function
 *
 * @package Theme\Components
 */
class SocialMenu extends Component {
	public function __construct( $args = array() ) { // phpcs:ignore
		$this->name     = 'social-menu';
		$this->template = "components/{$this->name}/{$this->name}";

		Enqueues::enqueue_component_styles( $this->name );

		// List of props passed to the template as variables
		$this->props = array_merge(
			array(
				'class'          => '', // Additional CSS class for the root element
				'title'          => '', // Text for the title element
				'theme_location' => '', // ID for the menu location
				'menu_class'     => '', // Additional CSS class to the menu element
			),
			$args
		);
	}
}
