<?php

namespace Theme\Components;

use Theme\Helpers\Component;

/**
 * A template for previewing the core theme styles and components, like colors, headings and buttons
 *
 * @package Theme\Components
 */
class StyleSpecs extends Component {
	public function __construct( $args = array() ) { // phpcs:ignore
		$this->name     = 'style-specs';
		$this->template = "components/{$this->name}/{$this->name}";
		$this->enqueue_scripts();

		global $wp_styles;

		// List of props passed to the template as variables
		$this->props = array_merge(
			array(
				'class'   => '', // Additional CSS class for the root element
				'css_uri' => $wp_styles->registered['theme']->src,
			),
			$args
		);
	}

	/**
	 * Enqueue scripts needed for the style specs to work
	 */
	private function enqueue_scripts() {
		wp_enqueue_script( // phpcs:ignore
			'vue',
			'https://cdn.jsdelivr.net/npm/vue@2.6.14/dist/vue.js',
			array(),
			null,
			false
		);

		$js_path = get_template_directory_uri() . "/components/{$this->name}/{$this->name}.js";
		wp_enqueue_script(
			'_B_THEME_NAME-style-specs-script',
			$js_path,
			array( 'vue' ),
			filemtime( get_template_directory( $js_path ) ),
			true
		);
	}
}
