<?php

namespace Theme\Components;

use Theme\Helpers\Component;
use Theme\Helpers\Enqueues;

/**
 * Heading of the page, with title and an optional description
 *
 * @package Theme\Components
 */
class PageHeader extends Component {
	public function __construct( $args = array() ) { // phpcs:ignore
		$this->name     = 'page-header';
		$this->template = "components/{$this->name}/{$this->name}";

		Enqueues::enqueue_component_styles( $this->name );

		// List of props passed to the template as variables
		$this->props = array_merge(
			array(
				'class'       => '', // Additional CSS class for the root element
				'title'       => '', // Text for the title
				'description' => '', // Optional intro text for the page
			),
			$args
		);
	}
}
