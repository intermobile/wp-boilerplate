<?php

namespace Theme\Helpers;

use Solidify\Core\Page;

/**
 * Base class for all pages
 *
 * @package Theme\Helpers
 */
abstract class Layout extends Page {
	/**
	 * Name of the layout
	 *
	 * @var string
	 */
	protected string $name;
}