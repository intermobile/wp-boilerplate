<?php

namespace Theme\Helpers;

use Solidify\Core\Component as ComponentBase;

/**
 * Base class for all components
 *
 * @package Theme\Helpers
 */
abstract class Component extends ComponentBase {
	/**
	 * Name of the component
	 *
	 * @var string
	 */
	protected string $name;
}