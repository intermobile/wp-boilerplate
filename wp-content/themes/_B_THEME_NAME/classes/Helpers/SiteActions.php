<?php

namespace Theme\Helpers;

/**
 * SiteActions class
 *
 * Class to define all controller actions from site
 *
 * @package Theme\Helpers
 */
class SiteActions {
	/**
	 * Calls the model and its respective action
     *
	 * @param object $request Request info.
	 */
	public static function controller_action( $request ) {
		// model.action_function
		if ( strpos( $request->site_action, '.' ) ) {
			header( 'Content-type: application/json; charset=utf-8' );
			$model_action = explode( '.', $request->site_action );
			$model        = 'Theme\\Models\\' . str_replace( '-', '', ucwords( $model_action[0], '_' ) );
			$action       = $model_action[1];
			$response     = null;
			try {
				$data = ( is_object( $request ) && isset( $request->data ) ) ? $request->data : null;
				if ( strlen( $action ) ) {
					$class = new $model( $action, $data );
					if ( method_exists( $class, $action ) ) {
						$class->$action();
					} else {
						throw new \Exception( "Method $action does not exist in " . ucwords( $model_action[0], '_' ) . ' model.' );
					}
					echo json_encode( $class->get_response() );
				} else {
					throw new \Exception( "Method $action does not exist" );
				}
			} catch ( \Exception $e ) {
				http_response_code( 500 );
				echo json_encode(
                    (object) array(
						'error'   => 'unexpected_issue',
						'message' => $e->getMessage(),
						'data'    => (object) array( 'status' => 500 ),
                    )
                );
			}
			exit();
		}
	}
}