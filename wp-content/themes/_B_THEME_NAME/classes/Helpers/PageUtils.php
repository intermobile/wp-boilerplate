<?php

namespace Theme\Helpers;

/**
 * A collection of utility functions to handle data from the page post type
 *
 * @package Theme\Helpers
 */
class PageUtils {
	/**
     * Returns an array with all parent pages from a page
	 *
     * @param int $page_id ID from the page.
     * @return WP_Post[]
     */
    public static function get_page_ascendancy( $page_id ) {
		$pages = array();
		$page  = get_post( $page_id );
        if ( $page ) {
			while ( $page->post_parent > 0 ) {
				$page    = get_post( $page->post_parent );
				$pages[] = $page;
			}
        }
        return $pages;
    }

	/**
     * Returns array of breadcrumbs of a page.
     *
     * @param int $page_id ID of the post.
     * @return object[] Object with 'name' and 'path' properties
     */
	public static function get_page_breadcrumbs( $page_id ): array {
		// Get parent pages
		$pages = self::get_page_ascendancy( $page_id );

		// Build breadcrumb objects
		$breadcrumbs = array();
		foreach ( array_reverse( $pages ) as $page ) {
			$breadcrumbs[] = (object) array(
				'title'     => $page->post_title,
				'permalink' => get_the_permalink( $page->ID ),
			);
		}
        return $breadcrumbs;
    }

	/**
     * Returns whether the current page uses a given template
	 *
     * @param string $template_name Name of the template without prefix and extension (i.e. 'events', 'register').
	 *
     * @return bool
     */
    public static function is_page_template( $template_name ) {
		// Check if it's an array instead of single string
		if ( is_array( $template_name ) ) {
			foreach ( $template_name as $template ) {
				if ( is_page_template( "template-{$template}.php" ) ) {
					return true;
				}
			}
			return false;
		} else { // Is single string
			return is_page_template( "template-{$template_name}.php" );
		}
    }

	/**
     * Returns the the registered page that uses a specific template
	 *
     * @param int $template_name Name of the template (i.e. 'events', 'register').
     * @return WP_Post|false
     */
    public static function get_page_with_template( $template_name ) {
		$pages = get_pages(
			array(
				'meta_key'   => '_wp_page_template',
				'meta_value' => "template-{$template_name}.php",
			)
		);
		return $pages ? $pages[0] : false;
    }
}