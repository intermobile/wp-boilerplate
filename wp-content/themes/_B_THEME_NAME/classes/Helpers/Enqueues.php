<?php

namespace Theme\Helpers;

/**
 * Util functions for enqueues
 *
 * @package Theme\Helpers
 */
class Enqueues {
	/**
	 * Return the path of a CSS file from the /assets/css folder
     *
	 * @param string $filename Name of the CSS file, without the extenstion.
	 */
	public static function get_admin_css_path( $filename ) {
		return get_template_directory() . "/assets/css/dist/admin/{$filename}.dist.css";
	}

	/**
	 * Return the URI of a CSS file from the /assets/css folder
     *
	 * @param string $filename Name of the CSS file, without the extenstion.
	 */
	public static function get_admin_css_uri( $filename ) {
		return get_template_directory_uri() . "/assets/css/dist/admin/{$filename}.dist.css";
	}

	/**
	 * Enqueue script and styles for a specific layout
     *
	 * @param string $layout Name of the layout in kebab case format.
	 * @param string $js_dependencies Additional dependencies for the layout script.
	 */
	public static function enqueue_page_assets( $layout, $js_dependencies = array() ) {
		if ( $layout ) {
			$css_path = "/assets/css/dist/layouts/{$layout}.dist.css";
			wp_enqueue_style(
				'_B_THEME_NAME-layout-styles',
				get_template_directory_uri() . $css_path,
				array( 'theme' ),
				filemtime( get_template_directory() . $css_path )
			);

			$js_deps = array_merge( $js_dependencies, array( 'jquery' /* Add here other global dependencies if needed */ ) );
			$js_path = get_template_directory_uri() . "/assets/js/dist/{$layout}.dist.js";
			wp_enqueue_script(
				'_B_THEME_NAME-layout-scripts',
				$js_path,
				$js_deps,
				filemtime( get_template_directory( $js_path ) ),
				true
			);
		}
	}

	/**
	 * Enqueue styles for a specific component type
     *
	 * @param string $component_name Name of the component in kebab case format.
	 */
	public static function enqueue_component_styles( $component_name ) {
		if ( $component_name ) {
			$css_path = "/assets/css/dist/components/{$component_name}.dist.css";
			wp_enqueue_style(
				"_B_THEME_NAME-{$component_name}-component-style",
				get_template_directory_uri() . $css_path,
				array( 'theme' ),
				filemtime( get_template_directory() . $css_path )
			);
		}
	}
}
