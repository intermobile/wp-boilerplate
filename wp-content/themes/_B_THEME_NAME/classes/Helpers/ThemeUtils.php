<?php

namespace Theme\Helpers;

/**
 * A collection of utility functions to be used between classes and templates
 *
 * @package Theme\Helpers
 */
class ThemeUtils {

	/**
	 * Prints the variable, object or array content with a <pre> tag and print_r()
     *
	 * @param any $to_print Variable with content to be printed.
	 */
	public static function showme( $to_print ) {
		echo '<pre class="showme">' . print_r( $to_print, true ) . '</pre>';
	}

	/**
	 * Prints a script to log in the console via javascript
     *
	 * @param string  $string Text to log in the console.
	 * @param boolean $with_script_tags Whether to include the <script> tags or not.
	 */
	public static function console_log( $string, $with_script_tags = true ) {
		$js_code = $string;
		if ( $with_script_tags ) {
			$js_code = '<script>' . $js_code . '</script>';
		}
		echo $js_code;
	}

	/**
	 * Prints a script to log in the console via javascript
     *
	 * @param string  $data object or array to log in the console as a JSON.
	 * @param boolean $label Text to appear below the log as a label.
	 * @param boolean $with_script_tags Whether to include the <script> tags or not.
	 */
	public static function console_log_json( $data, $label = '', $with_script_tags = true ) {
		if ( ! self::is_production() || isset( $_GET['enable_logs'] ) ) {
			$js_code = 'console.log(' . ( $label ? "`%c{$label}:`, `color:gray`, " : '' ) . wp_json_encode( $data, JSON_HEX_TAG ) . ');';
			if ( $with_script_tags ) {
				$js_code = '<script>' . $js_code . '</script>';
			}
			echo $js_code;
		}
	}

	/**
	 * Returns true if on production environment
     *
	 * @return bool Whether ENV is set as 'production' or not
	 */
	public static function is_production(): bool {
		if ( ! defined( 'ENV' ) ) {
			define( 'ENV', 'local' );
		}
		return ENV === 'production';
	}

	/**
	 * Returns true if on stage environment
     *
	 * @return bool Whether ENV is set as 'stage' or not
	 */
	public static function is_stage(): bool {
		if ( ! defined( 'ENV' ) ) {
			define( 'ENV', 'local' );
		}
		return ENV === 'stage';
	}

	/**
	 * Returns true if on homolog environment
     *
	 * @return bool Whether ENV is set as 'homolog' or not
	 */
	public static function is_homolog(): bool {
		if ( ! defined( 'ENV' ) ) {
			define( 'ENV', 'local' );
		}
		return ENV === 'homolog';
	}

	/**
	 * Returns true if on local environment
     *
	 * @return bool Whether ENV is set as 'local' or unset
	 */
	public static function is_local(): bool {
		return ! defined( 'ENV' ) || ENV === 'local';
	}

	/**
	 * Returns a string with the CSS file content
     *
	 * @param string $file CSS file path (without extension).
	 * @param array  $vars Variables to replace in the CSS file.
	 *
	 * @return array $vars Optional array with variables to be replaced. i.e. array( "heading-color" => "red" )
	 */
	public static function get_style_file_content( $file, $vars = array() ) {
		$file_path    = get_template_directory() . '/' . $file . '.css';
		$file_content = file_get_contents( $file_path );
		foreach ( $vars as $var => $value ) {
			$file_content = str_replace( ( '--' . $var ), $value, $file_content );
		}
		return $file_content;
	}
}