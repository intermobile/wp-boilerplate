<?php

namespace Theme\Helpers;

/**
 * A common structure for article data that extends the PostObject class
 *
 * @package Theme\Helpers
 */
class ArticleObject extends PostObject {

	/**
	 * Article thumbnail image data.
	 *
	 * @var ImageObject
	 */
	public $image;

	/**
	 * Name of the post parent category.
	 *
	 * @var string
	 */
	public $category;

	/**
	 * Formatted publishing date.
	 *
	 * @var string
	 */
	public $date;

	/**
	 * A class for storing article data
	 *
	 * @param string $post (Optional) Id of the post to get data from. If not provided will try to get post from current context.
	 */
	public function __construct( $post = null ) {
		parent::__construct( $post );

		// Get featured image
		$thumbnail_id = get_post_thumbnail_id( $this->id );
		$this->image  = new ImageObject( $thumbnail_id );

		// Get publishing date
		$this->date = get_the_date( '', $this->id );
	}

	/**
	 * Returns a list of articles with their respective object class
	 *
	 * @param array $posts Array of posts in WP_Post object format.
	 *
	 * @return ArticleObject[] List of articles data as ArticleObject.
     */
	public static function format_articles_data( $posts = array() ) {
		return array_map(
            function( $post ) {
				return new ArticleObject( $post );
			},
            $posts
        );
	}
}