<?php

namespace Theme\Helpers;
use WP_Query;

/**
 * A collection of functions for getting posts from database
 *
 * @package Theme\Helpers
 */
class PostQuery {

	/**
	 * Get a defined number of posts for pagination or load more functionality
	 *
	 * @param int   $quantity Number of posts to gather. If null, will get the default setting.
	 * @param array $args Additional args for the posts query. If provided params that are already in use inside this function (i.e. `posts_per_page`), the passed params will override the original.
	 *
	 * @return object Object with `post_data`, `number_posts` and `max_pages` properties.
	 */
	public static function get_posts( $quantity = null, $args = array(), $try_parent = false ) {

		if ( isset( $args['includes'] ) && count( $args['includes'] ) > 0 ) {
			$args = array_merge(
				$args,
				array(
					'orderby' => 'post__in',
					'include' => array_slice( $args['includes'], 0, $quantity ),
				)
			);
		}

		$query_args = array_merge(
			array(
				'posts_per_page' => $quantity ? $quantity : 12,
			),
			$args // Adds or overrides params
		);

		$post_data = new \WP_Query( $query_args );

		// If not enough, tries to fill with parent category posts
		if ( isset( $args['category'] ) && count( $post_data->posts ) < $quantity && $try_parent ) {
			$parent_category_posts = array();
			$exclude = array();
			// Diff in qty and excluding already filled posts
			foreach ( $post_data->posts as $post ) {
				$quantity--;
				$exclude[] = $post->id;
			}

			// Detecting parent category
			$child_category = get_category( $args['category'] );
			if ( isset( $child_category->parent ) && $child_category->parent > 0 ) {
				// Single recursive call (do not try with parent)
				$parent_category_posts = self::get_posts( $quantity, array('category' => $child_category->parent, 'exclude'  => $exclude, 'orderby'  => 'rand'), false );
			}
			$post_data->posts = array_merge( $post_data->posts, $parent_category_posts );
		}

		return (object) array(
			'posts'        => PostObject::format_posts_data( $post_data->posts ),
			'max_pages'    => $post_data->max_num_pages,
			'number_posts' => $post_data->found_posts,
		);
	}
}