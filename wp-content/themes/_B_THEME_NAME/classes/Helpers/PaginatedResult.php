<?php
/**
 * Pagination class
 *
 */

namespace Theme\Helpers;

use Theme\Components\PostCard;

class PaginatedResult {
	/**
	 * Returns post array
	 *
	 * @param array $paginated_data
	 * @param string $div_class
	 * @param string $component_class
	 * @return string
	 */
	public static function process_data( $paginated_data, $div_class, $component_class ): string {
		return self::html_block( $paginated_data, $div_class, $component_class );
	}

	/**
	 * Returns options to paginate
	 *
	 * @param array $paginated_data
	 * @param string $div_class
	 * @param string $component_class
	 * @return string
	 */
	public static function html_block( $paginated_data, $div_class, $component_class ): string {
		$card = '';
		foreach ( $paginated_data as $value ) {
			$card .= '<div class="' . $div_class . '">' . new PostCard(
                array(
					'class'     => $component_class,
					'post_data' => $value,
					'show_date' => false,
                )
            ) . '</div>';
		}

		return $card;
	}
}
