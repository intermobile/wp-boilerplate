<?php

namespace Theme\Helpers;

/**
 * A collection of functions for handling with post data
 *
 * @package Theme\Helpers
 */
class ArticleUtils {

	/**
	 * Returns an ImageObject instance with data from the post thumbnail
	 *
	 * @param int $post_id ID of the post to get thumbnail from.
     */
	public static function get_post_featured_image( $post_id ): ImageObject {
		$feat_image = null;
		if ( has_post_thumbnail( $post_id ) ) {
			$feat_image = new ImageObject( get_post_thumbnail_id( $post_id ) );
		}
		return $feat_image;
	}


	/**
	 * Returns author data for the post
	 *
	 * @param int $post_id ID of the post to get author data.
	 */
	public static function get_post_author_data( $post_id ): array {
		$author_id = get_post_field( 'post_author', $post_id );

		return array(
			'id'    => get_the_author_meta( 'ID', $author_id ),
			'name'  => get_the_author_meta( 'display_name', $author_id ),
			'image' => get_avatar( get_the_author_meta( 'ID', $author_id ) ),
		);
	}

	/**
     * Calc the estimated reading time for an article, based on the amount of words in the content.
     *
     * @param string $content Content of the article.
     * @return int Time in minutes
     */
	public static function get_post_reading_time( $content ): int {
		$words_count      = str_word_count( wp_strip_all_tags( $content ) );
		$words_per_minute = 220;
		$total_read_time  = $words_count / $words_per_minute;
		return ceil( $total_read_time );
	}

	/**
	 * Returns the WP_Term object from the post primary category
	 *
	 * @param int $post_id ID of the post.
	 */
	public static function get_post_primary_category( $post_id ): \WP_Term {
		if ( ! $post_id ) {
			return null;
		}

		$primary_id = intval( get_post_meta( $post_id, '_yoast_wpseo_primary_category', true ) );
		return get_category( $primary_id );
	}

	/**
     * Returns array of breadcrumbs of an article.
     *
     * @param int $post_id ID of the post.
     * @return object[] Objects array with 'name' and 'permalink' properties
     */
	public static function get_post_breadcrumbs( $post_id ): array {
		// Get parent categories
		$primary_category = self::get_post_primary_category( $post_id );
		$categories       = array();
		if ( $primary_category ) {
			$categories[] = $primary_category;
			if ( $primary_category->parent > 0 ) {
				$categories = array_merge( $categories, TaxonomyUtils::get_term_ascendancy( $primary_category->term_id, 'category' ) );
			}
		}

		// Build the breadcrumb objects
		$breadcrumbs = array();
		foreach ( array_reverse( $categories ) as $category ) {
			$breadcrumbs[] = (object) array(
				'title'     => $category->name,
				'permalink' => get_category_link( $category ),
			);
		}
		return $breadcrumbs;
	}
}