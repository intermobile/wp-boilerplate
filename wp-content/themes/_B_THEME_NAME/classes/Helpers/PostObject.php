<?php

namespace Theme\Helpers;

use WP_Post;

/**
 * A common structure for post data to be used in between classes and templates
 *
 * @package Theme\Helpers
 */
class PostObject {

	/**
	 * ID of the post
	 *
	 * @var int
	 */
	public $id;

	/**
	 * Slug of the post
	 *
	 * @var string
	 */
	public $slug;

	/**
	 * Title of the post
	 *
	 * @var string
	 */
	public $title;

	/**
	 * URL of the post page
	 *
	 * @var string
	 */
	public $permalink;

	/**
	 * Publishing time in standard format.
	 *
	 * @var string
	 */
	public $publish_time;

	/**
	 * Author data.
	 *
	 * @var object
	 */
	public $author;

	/**
	 * Constructor for the PostObject class
	 *
	 * @param int|WP_Post $post (Optional) Id or WP_Post object of the post to get data from. If not provided will try to get from current context.
	 */
	public function __construct( $post = null ) {
		$post_data = null;

		if ( null === $post ) {
			// As $post was not provided, get data from current context
			$post_data = get_post( get_the_ID() );

		} elseif ( is_int( $post ) ) {
			// If $post is an ID, get the post data object
			$post_data = get_post( $post );

		} elseif ( $post instanceof WP_Post ) {
			// Get data from WP_Post object
			$post_data = $post;
		}

		if ( $post_data instanceof WP_Post ) {
			// Get data from WP_Post object
			$this->title        = $post_data->post_title;
			$this->id           = $post_data->ID;
			$this->slug         = $post_data->post_name;
			$this->permalink    = get_permalink( $post_data->ID );
			$this->publish_time = $post_data->post_date;
			$this->author       = (object) array( 'id' => $post_data->post_author );
		}
	}

	/**
	 * Returns a list of posts with their respective object class
	 *
	 * @param array $posts Array of posts in WP_Post object format.
	 *
	 * @return any[] List of posts data, as ArticleObject or others.
	 */
	public static function format_posts_data( $posts = array() ) {
		return array_map(
			function( $post ) {
				// return new PostObject( $post );
				switch ( $post->post_type ) {
					case 'post':
					default:
						return new ArticleObject( $post );
				}
			},
			$posts
		);
	}
}