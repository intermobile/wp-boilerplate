<?php

namespace Theme\Helpers;

/**
 * A collection of functions for handling with menu data
 *
 * @package Theme\Helpers
 */
class MenuUtils {
	/**
	 * Returns structured data from a registered menu location
	 *
	 * @param $string $menu_location Menu location.
	 *
	 * @return object|null Menu data and items hierarchicaly ordered, or null if not found.
	 */
	public static function get_menu_data( $menu_location ) {
		$menu_object = wp_get_nav_menu_object( get_nav_menu_locations()[ $menu_location ] );
		if ( ! $menu_object ) {
			return null;
		}
		$menu_items = wp_get_nav_menu_items( $menu_object->term_id );
		$menu_data  = (object) array(
			'id'       => $menu_object->term_id,
			'name'     => $menu_object->name,
			'slug'     => $menu_object->slug,
			'location' => $menu_location,
			'count'    => $menu_object->count,
			'items'    => self::get_menu_items_hierarchically( $menu_items ),
		);
		return $menu_data;
	}

	/**
	 * Returns menu items stacked hierarchically
	 *
	 * @param array      $menu_items All menu items from wp_get_nav_menu_items().
	 * @param string|int $parent_item_id ID of the parent menu item to get items from, default is 0 (no parent).
	 *
	 * @return array
	 */
	public static function get_menu_items_hierarchically( $menu_items, $parent_item_id = 0 ) {
		$items = array();
		foreach ( $menu_items as $menu_item ) {
			if ( intval( $parent_item_id ) === intval( $menu_item->menu_item_parent ) ) {
				$items[] = self::get_menu_item( $menu_item, $menu_items );
			}
		}
		return $items;
	}

	/**
	 * Returns formatted data from a menu item
	 *
	 * @param WP_Post $menu_item Raw data from a menu item.
	 * @param array   $menu_items All menu items from wp_get_nav_menu_items().
	 *
	 * @return array
	 */
	public static function get_menu_item( $menu_item, $menu_items ) {
		$child_items = self::get_menu_items_hierarchically( $menu_items, $menu_item->ID );
		$item        = (object) array(
			'id'          => $menu_item->ID,
			'url'         => $menu_item->url,
			'title'       => $menu_item->title,
			'attr_title'  => $menu_item->attr_title,
			'class'       => join( ' ', $menu_item->classes ),
			'target'      => $menu_item->target,
			'rel'         => $menu_item->xfn,
			'child_items' => $child_items,
			'object_id'   => $menu_item->object_id,
			'object_type' => $menu_item->object,
		);
		return $item;
	}
}