<?php

namespace Theme\Hooks;

use Solidify\Core\Hook;

/**
 * A class of type Hook, that registers additional security handlers
 */
class Security extends Hook {
	public function __construct() { // phpcs:ignore
		$this->add_action( 'after_setup_theme', 'set_whiltelisted_domains' );
		$this->add_action( 'login_form', 'disable_login_autocomplete' );
	}

	/**
	 * Whitelist domains for the Content Security Policy header
	 */
	public function set_whiltelisted_domains() {
		$allowed_domains = join(
            ' ',
            array(
				'https://www.youtube.com/',
            )
        );

		header( "Content-Security-Policy: worker-src * blob:; child-src 'self' {$allowed_domains}" );
	}

	/**
	 * Disable autocomplete functionality in the WP login form
	 */
	public function disable_login_autocomplete() {
		$content = ob_get_contents();
		if (ob_get_contents()) ob_end_clean();
		$content = str_replace( 'id="user_login"', 'id="user_login" autocomplete="off"', $content );
		echo $content;
	}
}
