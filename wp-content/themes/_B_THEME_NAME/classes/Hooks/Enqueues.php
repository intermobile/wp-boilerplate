<?php

namespace Theme\Hooks;

use Solidify\Core\Hook;

/**
 * Enqueue assets for current template
 */
class Enqueues extends Hook {
	public function __construct() { // phpcs:ignore
		$this->add_action( 'wp_enqueue_scripts', 'enqueue_theme_assets' );
	}

	/**
	 * Enqueue scripts and styles for the general theme
	 */
	public function enqueue_theme_assets(): void {
		$css_path = get_template_directory_uri() . '/assets/css/dist/core/theme.dist.css';
		wp_enqueue_style(
            'theme',
            $css_path,
            array(),
            filemtime( get_template_directory( $css_path ) )
		);
	}
}