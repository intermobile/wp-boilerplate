<?php

namespace Theme\Hooks;

use Solidify\Core\Hook;

/**
 * A class of type Hook, that register custom image sizes for uploaded media
 */
class ImageSizes extends Hook {
	public function __construct() { // phpcs:ignore
		$this->add_action( 'after_setup_theme', 'images_sizes' );
	}

	/**
	 * Define the image sizes that will be used in the theme.
	 */
	public function images_sizes(): void {
		// SQUARE
		// add_image_size( '250x250', 250, 250, true ); // 1:1

		// HORIZONTAL
		// add_image_size( '300x400', 300, 400, true ); // 3:4

		// VERTICAL
		// add_image_size( '500x250', 500, 250, true ); // 2:1
	}
}
