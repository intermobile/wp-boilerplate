<?php

namespace Theme\Hooks;

use Solidify\Core\Hook;
use Theme\Components\PaginationButton;

/**
 * A class for handling pagination links
 */
class Pagination extends Hook {

	/**
	 * Handles any request for the 'PAGINATION' action
	 */
	public function handle_pagination_links($max_pages, $paged) {
		$links = array();

		if ( $paged >= 1 ) {
			$links[] = $paged;
		}

		if ( $paged >= 3 ) {
			$links[] = $paged - 1;
			$links[] = $paged - 2;
		}

		if ( ( $paged + 2 ) <= $max_pages ) {
			$links[] = $paged + 2;
			$links[] = $paged + 1;
		}

		echo new PaginationButton(
			array(
				$links,
				$max_pages,
				$paged,
			)
		);

		exit();
	}
}
