<?php

namespace Theme\Hooks;

use Solidify\Core\Hook;

/**
 * Hook functions to set up some configurations for the theme
 */
class Theme extends Hook {
	public function __construct() { // phpcs:ignore
        $this->add_action( 'after_setup_theme', 'setup_theme' );
		$this->add_action( 'wp_enqueue_scripts', 'enqueue_scripts' );
		$this->add_action( 'init', 'exclude_pages_from_search', 99 );
		$this->add_filter( 'body_class', 'add_classes_to_body' );
		$this->add_action( 'wp_head', 'add_pingback_url_header' );
	}

	/**
	 * Theme configurations
	 */
	public function setup_theme(): void {
		// Make theme available for translation.
		// Translations can be filed in the /languages/ directory.
		load_theme_textdomain( '_B_THEME_NAME', get_template_directory() . '/languages' );

		// Let WordPress manage the document title.
		// By adding theme support, we declare that this theme does not use a hard-coded <title> tag, and expect WordPress to provide it for us.
		add_theme_support( 'title-tag' );

		// Register menu locations to use in the theme
		register_nav_menus(
            array(
				'main-menu'   => 'Main Menu',
				// 'mobile-menu' => 'Mobile Menu',
				'social-menu' => 'Social Menu',
				'footer-menu' => 'Footer Menu',
            )
        );

		// Switch default core markup for search form, gallery and image captions to output valid HTML5.
		add_theme_support(
            'html5',
            array(
				'search-form',
				'gallery',
				'caption',
            )
        );

		// Enable thumbnails generation for media
		add_theme_support( 'post-thumbnails' );
	}

	/**
	 * Enqueue scripts for the front-end
	 */
	public function enqueue_scripts() {
		// Replace jQuery for a newer version
		wp_deregister_script( 'jquery' );
		wp_enqueue_script( 'jquery', get_template_directory_uri() . '/assets/js/libs/jquery-3.5.1.slim.min.js', array(), null, true );

		// Remove Gutemberg styles for any page except posts
		if ( ! is_single() ) {
			wp_dequeue_style( 'wp-block-library' );
		}

		// Remove unnecessary scripts
		wp_deregister_script( 'wp-embed' );
	}

	/**
	 * Adds custom classes to the array of body classes.
     *
	 * @param array $classes Classes for the body element.
	 */
	public function add_classes_to_body( $classes ): array {
		// Adds a class of hfeed to non-singular pages.
		if ( ! is_singular() ) {
			$classes[] = 'hfeed';
		}

		return $classes;
	}

	/**
	 * Exclude pages from WP search
	 */
	public function exclude_pages_from_search() {
		global $wp_post_types;
		$wp_post_types['page']->exclude_from_search = true;
	}

	/**
	 * Add a pingback url auto-discovery header for single posts, pages, or attachments
	 */
	public function add_pingback_url_header() {
		if ( is_singular() && pings_open() ) {
			echo '<link rel="pingback" href="', esc_url( get_bloginfo( 'pingback_url' ) ), '">';
		}
	}
}
