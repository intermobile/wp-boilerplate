<?php

namespace Theme\Hooks;

use Solidify\Core\Hook;
use Theme\Helpers\PostQuery;
use Theme\Helpers\PaginatedResult;

/**
 * A class for handling AJAX requests for loading more posts
 */
class LoadMore extends Hook {

	/**
	 * An array of arguments to generate query
	 *
	 * @var string
	 */
	private $args = array();

	/**
	 * An string to format html in return
	 *
	 * @var string
	 */
	private $div_class = '';

	/**
	 * An string to format html in return
	 *
	 * @var string
	 */
	private $component_class = '';


	/**
	 * Register the hooks for this class
	 */
	public function __construct() {
		$this->add_action( 'wp_ajax_load_more_posts', 'handle_load_more_posts' );
		$this->add_action( 'wp_ajax_nopriv_load_more_posts', 'handle_load_more_posts' );
	}

	/**
	 * Handles any request for the 'load_more_posts' AJAX action
	 */
	public function handle_load_more_posts() {
		// Get queried context
		check_ajax_referer( 'load_more_posts' );

		// Get common data for the ajax request
		$this->args  			= isset( $_POST['args'] ) ? sanitize_text_field( wp_unslash( $_POST['args'] ) ) : null;
		$this->div_class  		= isset( $_POST['div_class'] ) ? sanitize_text_field( wp_unslash( $_POST['div_class'] ) ) : null;
		$this->component_class  = isset( $_POST['component_class'] ) ? sanitize_text_field( wp_unslash( $_POST['component_class'] ) ) : null;

		$this->handle_posts();

		exit();
	}

	/**
	 * Handles requests for posts from a specific category (or subcategory)
	 *
	 */
	public function handle_posts() {
		$posts = PostQuery::get_posts(
			$this->args
		);
		echo json_encode(
			array(
				'html' => PaginatedResult::process_data(
					$posts['post_data'],
					$this->div_class,
					$this->component_class
				),
			)
		);
	}
}
