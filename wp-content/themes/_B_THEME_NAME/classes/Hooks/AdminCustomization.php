<?php

namespace Theme\Hooks;

use Solidify\Core\Hook;
use Theme\Helpers\ThemeUtils;
use Theme\Helpers\Enqueues;
use Theme\Helpers\PageUtils;

/**
 * Hooks for CMS customizations
 */
class AdminCustomization extends Hook {
	public function __construct() { // phpcs:ignore
		// Favicons
		$this->add_action( 'admin_head', 'custom_admin_favicon' );

		// Admin enqueues
		$this->add_action( 'admin_enqueue_scripts', 'enqueue_admin_styles' );
		$this->add_action( 'admin_enqueue_scripts', 'enqueue_custom_admin_scripts' );

		// List columns
		$this->add_filter( 'manage_posts_columns', 'add_post_admin_custom_columns', 2 );
		$this->add_action( 'manage_posts_custom_column', 'add_post_admin_custom_column_content', 5, 2 );
		$this->add_filter( 'manage_pages_columns', 'add_page_admin_custom_columns', 2 );
		$this->add_action( 'manage_pages_custom_column', 'add_page_admin_custom_column_content', 5, 2 );

		// Menu
		$this->add_action( 'admin_menu', 'add_homepage_admin_menu_item' );

		// Color scheme
		$this->add_action( 'get_user_option_admin_color', 'override_admin_color' );
		// Remove color scheme option as it is defined per environment
		remove_action( 'admin_color_scheme_picker', 'admin_color_scheme_picker' );
	}

	/**
	 * Define a custom favicon to the CMS
     */
	public static function custom_admin_favicon() {
		$favicon_url = get_stylesheet_directory_uri() . '/assets/img/_B_FAVICON.png';
		echo "<link rel=\"shortcut icon\" type=\"image/png\" href=\"{$favicon_url}\" />";
	}

	/**
	 * Enqueue styles to the admin pages
	 */
	public function enqueue_admin_styles() {
		wp_enqueue_style(
			'admin-global-styles',
			Enqueues::get_admin_css_uri( 'global' ),
			array(),
			filemtime( Enqueues::get_admin_css_path( 'global' ) )
		);
		wp_enqueue_style(
			'admin-fields-styles',
			Enqueues::get_admin_css_uri( 'fields' ),
			array(),
			filemtime( Enqueues::get_admin_css_path( 'fields' ) )
		);
		wp_enqueue_style(
			'admin-info-styles',
			Enqueues::get_admin_css_uri( 'info' ),
			array(),
			filemtime( Enqueues::get_admin_css_path( 'info' ) )
		);
	}

	/**
     * Scripts to help usability of fields
     *
     * @param string $hook Name of the hooked file.
     */
    public function enqueue_custom_admin_scripts( $hook ) {
		wp_enqueue_script(
			'admin-acf-utilities',
			get_template_directory_uri() . '/assets/js/admin/acf.js',
			array( 'jquery' ),
			filemtime( get_template_directory( '/assets/js/admin/acf.js' ) ),
            true
        );
    }

	/**
	 * Add column for extra page data to admin listings
     *
	 * @param array $columns Listing columns.
	 * @return array
	 */
	public function add_page_admin_custom_columns( $columns ) {
		$columns['slug'] = 'Slug';
		return $columns;
	}

	/**
	 * Add column for post thumbnail on admin listings
     *
	 * @param array $columns Listing columns.
	 * @return array
	 */
	public function add_post_admin_custom_columns( $columns ) {
		$columns['thumbnail'] = 'Featured Image';
		$columns['post_id']   = 'ID';
		return $columns;
	}

	/**
	 * Display extra page in the admin listing columns
     *
	 * @param array  $columns Listing columns.
	 * @param string $id Page ID.
	 */
	public function add_page_admin_custom_column_content( $columns, $id ) {
		switch ( $columns ) {
			case 'slug':
				$slug = get_post_field( 'post_name', $id );
				echo "<code>{$slug}</code>";
				break;

			default:
				break;
		}
	}

	/**
	 * Grabbing post thumbnail and displaying it in the column
     *
	 * @param array  $columns Listing columns.
	 * @param string $id Post ID.
	 */
	public function add_post_admin_custom_column_content( $columns, $id ) {
		switch ( $columns ) {
			case 'thumbnail':
				echo '<a href="' . get_post_permalink( $id ) . '" class="img-wrapper">';
				echo the_post_thumbnail( 'thumbnail' );
				echo '</a>';
				break;

			case 'post_id':
				echo "<strong class=\"post-id-column\">{$id}</strong>";
				break;

			default:
				break;
		}
	}

	/**
	 * Add item to the Home page in the main menu
	 */
	public function add_homepage_admin_menu_item() {
		foreach ( get_pages() as $page ) {
			if ( (int) get_option( 'page_on_front' ) === $page->ID ) {
				add_menu_page(
					$page->post_title,
					$page->post_title,
					'edit_pages',
					'post.php?post=' . $page->ID . '&action=edit',
					'',
					'dashicons-admin-home',
					2
				);
			}
		}
	}

	/**
	 * Override the CMS color scheme per environment
	 */
	public function override_admin_color() {
		if ( isset( $_COOKIE['wp_color_scheme'] ) ) {
			return wp_unslash( $_COOKIE['wp_color_scheme'] );
		} elseif ( ThemeUtils::is_production() ) {
			return 'modern';
		} elseif ( ThemeUtils::is_stage() ) {
			return 'light';
		} elseif ( ThemeUtils::is_homolog() ) {
			return 'ectoplasm';
		} else {
			return 'midnight';
		}
	}
}