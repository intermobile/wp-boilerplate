<?php

namespace Theme\Hooks;

use Solidify\Core\Hook;

/**
 * Hooks for automating changes when saving posts
 */
class PostSave extends Hook {
	public function __construct() { // phpcs:ignore
		$this->add_action( 'save_post', 'assign_parent_category' );
	}

	/**
	 * Auto check the parent category if a child category is selected
     *
	 * @param string $post_id ID of the post.
	 */
	public function assign_parent_category( $post_id ) {
		if ( isset( $post_id ) && 'post' !== get_post_type( $post_id ) ) {
			return $post_id;
        }

		// Get all assigned terms
		$terms = get_the_terms( $post_id, 'category' );
		foreach ( $terms as $term ) {
			if ( $term->parent !== 0 && ! has_term( $term->parent, 'category', $post_id ) ) {
				wp_set_object_terms( $post_id, array( $term->parent ), 'category', true );
				$term = get_term( $term->parent, 'category' );
			}
		}
	}
}