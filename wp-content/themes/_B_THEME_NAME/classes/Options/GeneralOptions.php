<?php

namespace Theme\Options;

use Solidify\Core\OptionsPage;

/**
 * Options page for website general options
 */
class GeneralOptions extends OptionsPage {
	public function __construct() { // phpcs:ignore
		$this->args = array(
			'page_title' => 'General Settings',
			'menu_title' => 'Options',
			'menu_slug'  => 'theme-options',
			'capability' => 'edit_posts',
			'redirect'   => false,
		);
	}
}
