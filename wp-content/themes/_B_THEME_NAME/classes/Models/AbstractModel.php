<?php

namespace Theme\Models;

/**
 * Model for handle users
 */
class AbstractModel {
	public $data;
	protected $_action;
	protected $_response_data;
	protected $_response_code;
	protected $_response_msg;
	public function __construct($action,$data=null) { // phpcs:ignore
		$this->_action = $action;
		$this->data    = $data;
	}
	public function set_response( $data = null, $code = null, $message = null ) {
		$this->_response_data = is_array( $data ) ? (object) $data : $data;
		$this->_response_code = $code;
		$this->_response_msg  = $message;
	}
	public function get_response() {
		$return_obj = $this->_response_data;
		if ( $this->_response_code ) {
			$return_obj = (object) array(
				'error'    => $this->_response_code,
				'message' => $this->_response_msg,
				'data'    => $this->_response_data,
			);
		}
		return $return_obj;
	}
}
