<?php

namespace Theme\Layouts;

use Theme\Helpers\Layout;
use Theme\Helpers\Enqueues;
use Theme\Helpers\PostQuery;
use Theme\Helpers\ThemeUtils;

/**
 * Class for managing data and template for the Home page
 *
 * @package Theme\Layouts
 */
class Home extends Layout {
	public function __construct( $args = array() ) { // phpcs:ignore
		$this->name     = 'home';
		$this->template = "layouts/{$this->name}/{$this->name}";

		Enqueues::enqueue_page_assets( $this->name );

		// List of props passed as variables to the template
		$this->props = array_merge(
			array(
				'site_name'       => get_bloginfo( 'name' ),
				'latest_articles' => PostQuery::get_posts( 6 ),
			),
			$args
		);

		ThemeUtils::console_log_json( $this->props, 'Layout Props' );
	}
}
