<?php

namespace Theme\Layouts;

use Theme\Helpers\Layout;
use Theme\Helpers\Enqueues;
use Theme\Helpers\PostQuery;
use Theme\Helpers\TaxonomyObject;
use Theme\Helpers\ThemeUtils;

/**
 * Class for managing the Category page data and template
 *
 * @package Theme\Layouts
 */
class Category extends Layout {
	/**
	 * Info from this category
	 *
	 * @var TaxonomyObject
	 */
	public $category;

	public function __construct( $args = array() ) { // phpcs:ignore
		$this->name     = 'category';
		$this->template = "layouts/{$this->name}/{$this->name}";

		Enqueues::enqueue_page_assets( $this->name );

		// Get category data
		global $cat;
		$category       = get_category( $cat );
		$this->category = new TaxonomyObject( $category );

		// List of props passed as variables to the template
		$this->props = array_merge(
			array(
				'category' => $category,
				'articles' => PostQuery::get_posts( 12, array( 'cat' => $cat ) ),
			),
			$args
		);

		ThemeUtils::console_log_json( $this->props, 'Layout Props' );
	}
}
