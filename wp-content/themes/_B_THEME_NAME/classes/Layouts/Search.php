<?php

namespace Theme\Layouts;

use Theme\Helpers\Layout;
use Theme\Helpers\ArticleObject;
use Theme\Helpers\Enqueues;
use Theme\Helpers\ThemeUtils;

/**
 * Class for managing the Search page data and template
 *
 * @package Theme\Layouts
 */
class Search extends Layout {
	/**
	 * Searched term
	 *
	 * @var string
	 */
    public $searched_term;

	public function __construct( $args = array() ) { // phpcs:ignore
		$this->name     = 'search';
		$this->template = "layouts/{$this->name}/{$this->name}";

		Enqueues::enqueue_page_assets( $this->name );

		global $wp_query;

		// Get term being searched
		$this->searched_term = urldecode( $wp_query->query['s'] );

		// List of props passed as variables to the template
		$this->props = array_merge(
			array(
				'searched_term' => $this->searched_term,
				'total_count'   => $wp_query->found_posts,
				'articles'      => ArticleObject::format_articles_data( $wp_query->posts ),
			),
			$args
		);

		ThemeUtils::console_log_json( $this->props, 'Layout Props' );
	}
}
