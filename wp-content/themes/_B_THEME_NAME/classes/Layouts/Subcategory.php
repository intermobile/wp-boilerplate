<?php

namespace Theme\Layouts;

use Theme\Helpers\Layout;
use Theme\Helpers\Enqueues;
use Theme\Helpers\PostQuery;
use Theme\Helpers\TaxonomyObject;
use Theme\Helpers\ThemeUtils;

/**
 * Class for managing the Subcategory page data and template
 *
 * @package Theme\Layouts
 */
class Subcategory extends Layout {
	/**
	 * Info from this subcategory
	 *
	 * @var TaxonomyObject
	 */
	public $subcategory;

	public function __construct( $args = array() ) { // phpcs:ignore
		$this->name     = 'subcategory';
		$this->template = "layouts/{$this->name}/{$this->name}";

		Enqueues::enqueue_page_assets( $this->name );

		global $cat;

		// Get category data
		$category          = get_category( $cat );
		$this->subcategory = new TaxonomyObject( $category );

		// List of props passed as variables to the template
		$this->props = array_merge(
			array(
				'subcategory' => $category,
				'articles'    => PostQuery::get_posts( -1, array( 'cat' => $cat ) ),
			),
			$args
		);

		ThemeUtils::console_log_json( $this->props, 'Layout Props' );
	}
}
