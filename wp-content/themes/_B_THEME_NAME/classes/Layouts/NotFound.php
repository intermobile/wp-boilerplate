<?php

namespace Theme\Layouts;

use Theme\Helpers\Layout;
use Theme\Helpers\Enqueues;
use Theme\Helpers\ThemeUtils;

/**
 * Class for managing the 404 page data and template
 *
 * @package Theme\Layouts
 */
class NotFound extends Layout {
	public function __construct( $args = array() ) { // phpcs:ignore
		$this->name     = '404';
		$this->template = "layouts/{$this->name}/{$this->name}";

		Enqueues::enqueue_page_assets( $this->name );

		// List of props passed as variables to the template
		$this->props = array_merge(
			array(),
			$args
		);

		ThemeUtils::console_log_json( $this->props, 'Layout Props' );
	}
}
