<?php

namespace Theme\Layouts;

use Theme\Helpers\Enqueues;
use Theme\Helpers\ThemeUtils;

/**
 * Class for managing data and tempplate for the Default page template
 *
 * @package Theme\Layouts
 */
class Page extends \Solidify\Core\Page {
	public function __construct( $args = array() ) { // phpcs:ignore
		$this->name     = 'page';
		$this->template = "layouts/{$this->name}/{$this->name}";

		Enqueues::enqueue_page_assets( $this->name );

		// List of props passed as variables to the template
		$this->props = array_merge(
			array(
				'page_title' => get_the_title(),
				'content'    => get_the_content(),
			),
			$args
		);

		ThemeUtils::console_log_json( $this->props, 'Layout Props' );
	}
}
