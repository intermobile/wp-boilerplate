<?php

namespace Theme\Layouts;

use Theme\Helpers\Layout;
use Theme\Helpers\ArticleObject;
use Theme\Helpers\Enqueues;
use Theme\Helpers\ThemeUtils;

/**
 * Class for managing the Article page data and template
 *
 * @package Theme\Layouts
 */
class Article extends Layout {
	/**
	 * Info from this article
	 *
	 * @var ArticleObject
	 */
	public $article;

	public function __construct( $args = array() ) { // phpcs:ignore
		$this->name     = 'article';
		$this->template = "layouts/{$this->name}/{$this->name}";

		Enqueues::enqueue_page_assets( $this->name );

		// Get article data and add other needed info
		$this->article              = new ArticleObject( get_the_ID() );
		$article_data               = $this->article;
		$article_data->content      = get_the_content();
		$article_data->author->name = get_the_author_meta( 'display_name', $article_data->author->id );

		// List of props passed as variables to the template
		$this->props = array_merge(
			array(
				'article_data' => $this->article_data,
			),
			$args
		);

		ThemeUtils::console_log_json( $this->props, 'Layout Props' );
	}
}
